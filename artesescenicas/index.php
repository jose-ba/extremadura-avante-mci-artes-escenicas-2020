<?php

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Http\Request;
use LiveCodeCoverage\LiveCodeCoverage; // Para poder generar code coverage con Laravel dusk


define('LARAVEL_START', microtime(true));

/*
|--------------------------------------------------------------------------
| Check If Application Is Under Maintenance
|--------------------------------------------------------------------------
|
| If the application is maintenance / demo mode via the "down" command we
| will require this file so that any prerendered template can be shown
| instead of starting the framework, which could cause an exception.
|
*/

if (file_exists(__DIR__.'/../laravel_artesescenicas/storage/framework/maintenance.php')) {
    require __DIR__.'/../laravel_artesescenicas/storage/framework/maintenance.php';
}

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| this application. We just need to utilize it! We'll simply require it
| into the script here so we don't need to manually load our classes.
|
*/

require __DIR__.'/../laravel_artesescenicas/vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request using
| the application's HTTP kernel. Then, we will send the response back
| to this client's browser, allowing them to enjoy our application.
|
*/

$app = require_once __DIR__.'/../laravel_artesescenicas/bootstrap/app.php';

$app->bind('path.public', function() {
    return __DIR__;
});

$kernel = $app->make(Kernel::class);

// Leo "manualmente" el .env porque en este punto todavía no se ha cargado y es
// necesario detectar si hay que obtener el code coverage o no. Esto no afecta
// al resto de la ejecución, no se carga nada en memoria salvo la variable que 
// nos interesa.

$envVars = \Dotenv\Dotenv::parse(file_get_contents($app->environmentFilePath()));

// Se arranca la captura de code coverage si está habilitado en el .env correspondiente

$shutDownCodeCoverage = LiveCodeCoverage::bootstrap(
    (bool)($envVars['CODE_COVERAGE_ENABLED'] ?? false),
    __DIR__ . '/../laravel_artesescenicas/tests/_dusk_output/',
    __DIR__ . '/../laravel_artesescenicas/phpunit.xml'
);


$response = tap($kernel->handle(
    $request = Request::capture()
))->send();

$kernel->terminate($request, $response);

// Se cierra el recolector de code coverage
$shutDownCodeCoverage();
