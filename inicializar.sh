#!/bin/bash

echo "Nombre del proyecto (en minusculas, solo letras y barras bajas). Se usará en rutas y en partes del código:"
read nuevoNombreProyecto

echo "Se va a crear la base de datos. Por favor, introduce la contraseña de mysql:"
read -s rootpasswd
mysql -uroot -p${rootpasswd} -e "CREATE DATABASE eavante_$nuevoNombreProyecto /*\!40100 DEFAULT CHARACTER SET utf8 */;"

echo "Configurando parte pública"
sed -i "s/laravel_nombreproyecto/laravel_$nuevoNombreProyecto/g" nombreproyecto/index.php

echo "Configurando server"
sed -i "s/nombreproyecto/$nuevoNombreProyecto/g" laravel_nombreproyecto/server.php

echo "Configurando artisan"
sed -i "s/nombreproyecto/$nuevoNombreProyecto/g" laravel_nombreproyecto/artisan

echo "Configurando Laravel mix"
sed -i "s/nombreproyecto/$nuevoNombreProyecto/g" laravel_nombreproyecto/webpack.mix.js

echo "Configurando .env"
cp laravel_nombreproyecto/.env.example laravel_nombreproyecto/.env
#sed -i "s/DB_PASSWORD=/DB_PASSWORD=bttcr/g" laravel_nombreproyecto/.env
sed -i "s/DB_DATABASE=laravel_nombreproyecto/DB_DATABASE=eavante_$nuevoNombreProyecto/g" laravel_nombreproyecto/.env

echo "Vaciando vendor y node_modules"
rm -rf laravel_nombreproyecto/vendor
rm -rf laravel_nombreproyecto/node_modules

echo "Renombrando carpetas"
mv nombreproyecto $nuevoNombreProyecto
mv laravel_nombreproyecto laravel_$nuevoNombreProyecto

rm $nuevoNombreProyecto/storage
ln -s ../laravel_$nuevoNombreProyecto/storage/app/public/ $nuevoNombreProyecto/storage

echo "Instalando dependencias"
cd laravel_$nuevoNombreProyecto || exit # Así evito que siga el script si falla cd
composer update  --with-all-dependencies
npm install

php artisan key:generate
php artisan migrate:refresh --seed
php artisan dusk:install

echo "Terminado"
