const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('../artesescenicas/');


mix.js([
    '../artesescenicas/assets/js/jquery.min.js',
    'resources/js/animarEnViewport.js',
    'resources/js/app.js'
], 'bpanel_assets/js')
    .postCss('resources/css/app.css', 'bpanel_assets/assets/css', [
        require('postcss-import'),
        require('tailwindcss'),
    ]);

mix.js([
    'resources/js/publicApp.js',
    'resources/js/animarEnViewport.js',
    'node_modules/slick-carousel/slick/slick.min.js'
], 'js/publicApp.js')

mix.sass('resources/bpanel/scss/style.scss', 'bpanel_assets/assets/css/bpanel-inspinia.css');
mix.styles([
    '../artesescenicas/bpanel_assets/assets/css/bootstrap.min.css',
    '../artesescenicas/bpanel_assets/assets/font-awesome/css/font-awesome.css',
    '../artesescenicas/bpanel_assets/assets/css/plugins/toastr/toastr.min.css',
    '../artesescenicas/bpanel_assets/assets/js/plugins/gritter/jquery.gritter.css',
    '../artesescenicas/bpanel_assets/assets/css/animate.css',
    '../artesescenicas/bpanel_assets/assets/css/bpanel-inspinia.css'
], '../artesescenicas/bpanel_assets/assets/css/bpanel.css');



mix.sass('resources/scss/estilo.scss', 'assets/css/estilo.css');


mix.styles([
    'node_modules/popup-simple/style.css',
    '../artesescenicas/assets/twbs/bootstrap/dist/css/bootstrap.css',
    'node_modules/slick-carousel/slick/slick.css',
    'node_modules/animate.css/animate.css',
    'node_modules/slick-carousel/slick/slick-theme.css',
    '../artesescenicas/assets/css/estilo.css'
], '../artesescenicas/assets/css/app.css');
