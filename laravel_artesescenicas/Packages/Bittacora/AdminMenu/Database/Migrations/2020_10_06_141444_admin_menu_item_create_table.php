<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdminMenuItemCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_menu_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('menu_id')->unsigned();
            $table->bigInteger('parent_id')->unsigned()->nullable(true);
            $table->boolean('active')->default(true);
            $table->string('title');
            $table->string('route_name');
            $table->string('permission');
            $table->tinyInteger('show_in_menu')->unsigned()->default(0);
            $table->tinyInteger('show_in_dashboard')->unsigned()->default(0);
            $table->integer('order_column');
            $table->string('icon')->nullable(true);
            $table->string('css_class')->nullable(true);
            $table->timestamps();

            $table->foreign('menu_id')->references('id')->on('admin_menu')->cascadeOnDelete();
            $table->foreign('parent_id')->references('id')->on('admin_menu_item')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin_menu_item');
    }
}
