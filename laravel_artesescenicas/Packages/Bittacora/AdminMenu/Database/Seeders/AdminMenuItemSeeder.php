<?php

namespace Packages\Bittacora\AdminMenu\Database\Seeders;

use Illuminate\Database\Seeder;
use Packages\Bittacora\AdminMenu\Models\AdminMenu;
use Packages\Bittacora\AdminMenu\Models\AdminMenuItem;

class AdminMenuItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sidebarAdminMenu = AdminMenu::where('slug', 'sidebar')->firstOrFail();

        $parentIdAdminMenuItem = AdminMenuItem::create([
            'menu_id' => $sidebarAdminMenu->id,
            'title' => 'Configuración bPanel',
            'route_name' => '',
            'permission' => '',
            'show_in_menu' => true,
            'show_in_dashboard' => false,
            'icon' => 'fa fa-cog'
        ]);
    }
}
