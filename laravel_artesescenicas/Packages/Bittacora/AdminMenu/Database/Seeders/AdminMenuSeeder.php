<?php

namespace Packages\Bittacora\AdminMenu\Database\Seeders;

use Illuminate\Database\Seeder;
use Packages\Bittacora\AdminMenu\Models\AdminMenu;

class AdminMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $headerIdMenu = AdminMenu::create([
            'slug' => 'header',
            'active' => true
        ]);


        AdminMenu::create([
            'slug' => 'sidebar',
            'active' => true
        ]);

        AdminMenu::create([
            'slug' => 'footer',
            'active' => true
        ]);
    }
}
