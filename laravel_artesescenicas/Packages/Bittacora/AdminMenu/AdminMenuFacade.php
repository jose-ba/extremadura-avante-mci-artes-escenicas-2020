<?php

declare(strict_types=1);

namespace Packages\Bittacora\AdminMenu;

use Packages\Bittacora\AdminMenu\Models\AdminMenu;
use Packages\Bittacora\AdminMenu\src\Infrastructure\AdminMenuRepository;

/**
 * Fachada para el módulo de menús para el panel de administración de bPanel4.
 * @package Packages\Bittacora\AdminMenu
 */
class AdminMenuFacade
{
    /**
     * @var AdminMenuRepository
     */
    private $adminMenuRepository;

    public function __construct(AdminMenuRepository $adminMenuRepository)
    {
        $this->adminMenuRepository = $adminMenuRepository;
    }

    /**
     * @param string $menuSlug Slug del menú que se va a crear
     * @return int Id del menú creado
     */
    public function createMenu(string $menuSlug): int
    {
        return $this->adminMenuRepository->create($menuSlug);
    }

    /**
     * Obtiene un menú a partir del slug
     * @param string $menuSlug
     * @return AdminMenu
     */
    public function getMenuBySlug(string $menuSlug): AdminMenu
    {
        return $this->adminMenuRepository->getMenu($menuSlug);
    }
}
