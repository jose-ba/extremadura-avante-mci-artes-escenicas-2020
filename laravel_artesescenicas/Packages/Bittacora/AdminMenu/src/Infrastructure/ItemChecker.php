<?php

declare(strict_types=1);

namespace Packages\Bittacora\AdminMenu\src\Infrastructure;

use App\Models\User;

/**
 * Comprueba determinadas condiciones del menú.
 * Class ChildrenChecker
 * @package Packages\Bittacora\AdminMenu\src\Infrastructure
 */
class ItemChecker
{
    /**
     * @param array $menuItem
     * @return bool
     */
    public function itemHasNoChildren(array $menuItem): bool
    {
        return empty($menuItem['children']) and empty($menuItem['route_name']);
    }


    /**
     * @param array $menuItem
     * @param User|null $user
     * @return bool
     */
    public function userHasRequiredPermission(array $menuItem, ?User $user): bool
    {
        $permission = (string)$menuItem['permission'];
        return empty($permission) or (!empty($user) and $user->can($permission));
    }
}
