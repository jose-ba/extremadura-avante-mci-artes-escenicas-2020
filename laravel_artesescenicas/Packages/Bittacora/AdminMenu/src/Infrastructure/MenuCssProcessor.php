<?php

declare(strict_types=1);

namespace Packages\Bittacora\AdminMenu\src\Infrastructure;

class MenuCssProcessor
{
    /**
     * @param array $menu
     * @param string $currentRouteName
     * @return bool
     */
    public function markActiveItems(array &$menu, string $currentRouteName = ''): bool
    {
        $hasActiveChildren = false;
        /**
         * @var array{route_name: string, children:array, css_class:string} $menuItem
         */
        foreach ($menu as &$menuItem) {
            if ($currentRouteName === $menuItem['route_name'] or
                $this->markActiveItems($menuItem['children'], $currentRouteName)) {
                $menuItem['css_class'] .= ' active';
                $hasActiveChildren = true;
            }
        }
        return $hasActiveChildren;
    }
}
