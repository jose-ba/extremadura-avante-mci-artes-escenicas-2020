<?php

declare(strict_types=1);

namespace Packages\Bittacora\AdminMenu\src\Infrastructure;

use Packages\Bittacora\AdminMenu\Models\AdminMenu;
use Packages\Bittacora\AdminMenu\Models\AdminMenuItem;

class AdminMenuRepository
{

    /**
     * @param string $slug
     * @param bool $active
     * @return int
     * @throws \Throwable Excepción si no se puede crear el menú.
     */
    public function create(string $slug, bool $active = true): int
    {
        $adminMenu = new AdminMenu();
        $adminMenu->slug = $slug;
        $adminMenu->active = $active;
        $adminMenu->saveOrFail();

        return $adminMenu->id;
    }

    /**
     * @param string $slug
     * @return AdminMenu
     */
    public function getMenu(string $slug): AdminMenu
    {
        return AdminMenu::where('slug', $slug)->firstOrFail();
    }
}
