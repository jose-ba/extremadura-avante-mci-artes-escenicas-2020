<?php

declare(strict_types=1);

namespace Packages\Bittacora\AdminMenu\src\Infrastructure;

use Illuminate\Support\Facades\Auth;
use Packages\Bittacora\AdminMenu\Models\AdminMenu;
use Packages\Bittacora\AdminMenu\Models\AdminMenuItem;

class MenuRepository
{
    /**
     * @param string $slug
     * @return array<array-key, mixed>
     */
    public function getMenu(string $slug): array
    {
        $adminMenu = AdminMenu::where('slug', $slug)->firstOrFail();

        $sidebarMenu = AdminMenuItem::where([
            'menu_id' => $adminMenu->id,
            'parent_id' => null,
            'active' => 1
        ])->get()->toArray();

        return $sidebarMenu;
    }
}
