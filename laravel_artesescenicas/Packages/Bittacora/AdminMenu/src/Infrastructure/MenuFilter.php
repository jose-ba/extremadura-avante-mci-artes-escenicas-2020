<?php

declare(strict_types=1);

namespace Packages\Bittacora\AdminMenu\src\Infrastructure;

use App\Models\User;

class MenuFilter
{

    /**
     * @var ItemChecker
     */
    private $itemChecker;
    /**
     * @var MenuCssProcessor
     */
    private $menuCssProcessor;

    public function __construct(ItemChecker $childrenProcessor, MenuCssProcessor $menuCssProcessor)
    {
        $this->itemChecker = $childrenProcessor;
        $this->menuCssProcessor = $menuCssProcessor;
    }

    /**
     * @param array<string, array<array-key, mixed>> $menu
     * @param User $user
     * @return array
     * @psalm-suppress ReferenceConstraintViolation
     */
    public function removeMenuElements(array &$menu, User $user): array
    {
        foreach ($menu as $k => &$menuItem) {
            $menu = $this->prepareMenuItemChildren($menuItem, $user, $menu, $k);
        }
        return $menu;
    }

    /**
     * @param array $menu
     * @param string $currentRouteName
     * @return bool
     */
    public function markActiveItems(array &$menu, string $currentRouteName = ''): bool
    {
        $hasActiveChildren = false;
        /**
         * @var array{route_name: string, children:array, css_class:string} $menuItem
         */
        foreach ($menu as &$menuItem) {
            if ($currentRouteName === $menuItem['route_name'] or
                $this->menuCssProcessor->markActiveItems($menuItem['children'], $currentRouteName)) {
                $menuItem['css_class'] .= ' active';
                $hasActiveChildren = true;
            }
        }
        return $hasActiveChildren;
    }

    /**
     * @param array $menuItem
     * @param User $user
     * @param array $menu
     * @param string|int $k
     * @return array
     */
    protected function prepareMenuItemChildren(array &$menuItem, User $user, array $menu, $k): array
    {
        if ($this->itemChecker->itemHasNoChildren($menuItem) or
            !$this->itemChecker->userHasRequiredPermission($menuItem, $user)) {
            unset($menu[$k]);
            return $menu;
        }

        /** @var array<string,array> $children */
        $children = $menuItem['children'];
        $menuItem['children'] = $this->removeMenuElements($children, $user);

        return $menu;
    }
}
