<?php

declare(strict_types=1);

namespace Packages\Bittacora\AdminMenu\Providers;

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Packages\Bittacora\AdminMenu\Middlewares\AdminMenuMiddleware;

class AdminMenuServiceProvider extends ServiceProvider
{
    public function register()
    {
    }

    public function boot(Router $router): void
    {
        $router->group(['middleware' => ['web']], function () {
            $this->loadRoutesFrom(__DIR__ . '/../Routes/routes.php');
        });
        $this->loadViewsFrom(__DIR__ . '/../Views', 'AdminMenu');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Seeders');
        $this->registerPackageMiddlewares();
    }

    /**
     * Registro el middleware para los menús.
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function registerPackageMiddlewares(): void
    {
        $router = $this->app->make(Router::class);
        $router->pushMiddlewareToGroup('web', AdminMenuMiddleware::class);
        $kernel = $this->app->make(Kernel::class);
        $kernel->appendMiddlewareToGroup('web', AdminMenuMiddleware::class);
    }
}
