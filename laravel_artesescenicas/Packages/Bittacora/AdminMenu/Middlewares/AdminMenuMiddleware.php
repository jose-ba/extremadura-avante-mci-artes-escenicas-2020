<?php

declare(strict_types=1);

namespace Packages\Bittacora\AdminMenu\Middlewares;

use App\Models\User;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\View\Factory;
use Packages\Bittacora\AdminMenu\src\Infrastructure\MenuCssProcessor;
use Packages\Bittacora\AdminMenu\src\Infrastructure\MenuFilter;
use Packages\Bittacora\AdminMenu\src\Infrastructure\AdminMenuRepository;
use RuntimeException;
use function is_object;

class AdminMenuMiddleware
{

    /**
     * @var AdminMenuRepository
     */
    private $menuRepository;
    /**
     * @var MenuFilter
     */
    private $menuFilter;
    /**
     * @var Guard
     */
    private $auth;
    /**
     * @var Factory
     */
    private $view;
    /**
     * @var MenuCssProcessor
     */
    private $menuCssProcessor;

    public function __construct(
        AdminMenuRepository $menuRepository,
        MenuFilter $menuFilter,
        MenuCssProcessor $menuCssProcessor,
        Guard $auth,
        Factory $view
    ) {
        $this->menuRepository = $menuRepository;
        $this->menuFilter = $menuFilter;
        $this->auth = $auth;
        $this->view = $view;
        $this->menuCssProcessor = $menuCssProcessor;
    }

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $menu = $this->menuRepository->getMenu('sidebar');
        $menu = $menu->toArray();
        /** @var array{children:array} $menu */
        $menu = $menu['children'];
        /** @var User|null $user */
        $user = $this->auth->user(); // Lo saco a una variable para no ejecutarlo en cada iteración
        if ($user instanceof User) {
            /** @var array<string, array<array-key, mixed>> $menu */
            $menu = $this->menuFilter->removeMenuElements($menu, $user);
        }
        $route = $request->route();
        if (!is_object($route) or !is_a($route, Route::class)) {
            throw new RuntimeException('La ruta a la que has intentado acceder no es válida.');
        }
        $currentRouteName = $route->getName() ?? '';
        $this->menuCssProcessor->markActiveItems($menu, $currentRouteName);
        $this->view->share('sidebarMenu', $menu);
        return $next($request);
    }
}
