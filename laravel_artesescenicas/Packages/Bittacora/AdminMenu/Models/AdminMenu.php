<?php

namespace Packages\Bittacora\AdminMenu\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminMenu
 * @package Packages\Bittacora\AdminMenu\Models
 * @property int $id
 * @property bool $active
 * @property string $slug
 */
class AdminMenu extends Model
{
    use HasFactory;

    public $table = 'admin_menu';

    public $fillable = ['slug', 'active'];

    protected $with = ['children'];

    public function children(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(AdminMenuItem::class, 'menu_id')->whereNull('parent_id');
    }
}
