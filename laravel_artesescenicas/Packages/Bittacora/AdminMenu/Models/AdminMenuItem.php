<?php

namespace Packages\Bittacora\AdminMenu\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

/**
 * Class AdminMenuItem
 * @package Packages\Bittacora\AdminMenu\Models
 * @property int $id
 * @property int $parent_id
 * @property AdminMenuItem[] $children
 * @property string $route_name
 */
class AdminMenuItem extends Model implements Sortable
{
    use HasFactory;
    use SortableTrait;

    public $table = 'admin_menu_item';
    protected $with = ['children'];
    public $fillable = [
        'menu_id',
        'parent_id',
        'active',
        'title',
        'route_name',
        'permission',
        'show_in_menu',
        'show_in_dashboard',
        'order_column',
        'icon',
        'css_class'
    ];

    protected $appends = ['children'];

    public function children(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(AdminMenuItem::class, 'parent_id');
    }

    public function buildSortQuery(): Builder
    {
        return static::query()->where('parent_id', $this->parent_id);
    }

    public function getChildrenAttribute(): Collection
    {
        return AdminMenuItem::where('parent_id', $this->id)->get();
    }
}
