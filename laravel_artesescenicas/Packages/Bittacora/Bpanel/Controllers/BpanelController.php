<?php

declare(strict_types=1);

namespace Packages\Bittacora\Bpanel\Controllers;

use Illuminate\View\View;

class BpanelController
{
    public function index(): View
    {
        return view('dashboard', ['pageTitle' => 'Panel de control']);
    }
}
