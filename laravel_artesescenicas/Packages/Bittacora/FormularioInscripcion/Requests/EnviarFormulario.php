<?php

declare(strict_types=1);

namespace Packages\Bittacora\FormularioInscripcion\Requests;

use App\Rules\MaxWordsRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class EnviarFormulario extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'empresa' => 'required|unique:inscripciones,empresa|max:100',
            'persona_contacto' => 'required|max:100',
            'cargo' => 'required|max:100',
            'web' => 'required|url|max:100',
            'facebook' => 'url|max:100|nullable',
            'twitter' => 'url|max:100|nullable',
            'youtube' => 'url|max:100|nullable',
            'instagram' => 'url|max:100|nullable',
            'pinterest' => 'url|max:100|nullable',
            'sector_actividad' => 'required|max:100',
            'principales_mercados' => 'required|max:100',
            'mercados_exclusividad' => 'required|max:100',
            'marcas' => 'required|max:100',
            'por_que_elegir_mi_empresa_es' => [
                'required',
                new MaxWordsRule(80)
            ],
            'por_que_elegir_mi_empresa_en' => [
                'required',
                new MaxWordsRule(80)
            ],
            'companias_teatro_danza' => 'nullable',
            'companias_teatro_musical' => 'nullable',
            'companias_teatro_drama' => 'nullable',
            'companias_teatro_intriga' => 'nullable',
            'companias_teatro_comedia' => 'nullable',
            'companias_teatro_literario' => 'nullable',
            'companias_teatro_historico' => 'nullable',
            'companias_teatro_infantil' => 'nullable',
            'companias_teatro_acrobatico' => 'nullable',
            'companias_teatro_otros' => 'nullable',
            'companias_teatro_otros_texto' => 'nullable|max:100',
            'distribuidores_teatro_danza' => 'nullable',
            'distribuidores_teatro_musical' => 'nullable',
            'distribuidores_teatro_drama' => 'nullable',
            'distribuidores_teatro_intriga' => 'nullable',
            'distribuidores_teatro_comedia' => 'nullable',
            'distribuidores_teatro_literario' => 'nullable',
            'distribuidores_teatro_historico' => 'nullable',
            'distribuidores_teatro_infantil' => 'nullable',
            'distribuidores_teatro_acrobatico' => 'nullable',
            'distribuidores_teatro_otros' => 'nullable',
            'distribuidores_teatro_otros_texto' => 'nullable|max:100',
            'programadores_festivales_danza' => 'nullable',
            'programadores_festivales_musical' => 'nullable',
            'programadores_festivales_drama' => 'nullable',
            'programadores_festivales_intriga' => 'nullable',
            'programadores_festivales_comedia' => 'nullable',
            'programadores_festivales_literario' => 'nullable',
            'programadores_festivales_historico' => 'nullable',
            'programadores_festivales_infantil' => 'nullable',
            'programadores_festivales_acrobatico' => 'nullable',
            'programadores_festivales_otros' => 'nullable',
            'programadores_festivales_otros_texto' => 'nullable|max:100',
            'otros' => 'nullable',
            'otros_texto' => 'nullable|max:100',
            'logo' => 'required|file|mimes:jpeg,png|max:5120|dimensions:min_width=800,min_height=800',
            'image' => 'nullable|file|mimes:jpeg,png|max:5120|dimensions:min_width=1200,min_height=1200',
            'privacidad' => 'required',
        ];
    }
}
