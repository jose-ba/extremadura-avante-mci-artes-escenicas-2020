<?php

declare(strict_types=1);

namespace Packages\Bittacora\FormularioInscripcion\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ActualizarInscripcion
 * @package Packages\Bittacora\FormularioInscripcion\Requests
 * @psalm-suppress PropertyNotSetInConstructor
 */
class ActualizarInscripcion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
