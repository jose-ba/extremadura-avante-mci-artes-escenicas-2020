<?php

declare(strict_types=1);

namespace Packages\Bittacora\FormularioInscripcion;

use AdvancedJsonRpc\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Facade;
use Packages\Bittacora\FormularioInscripcion\Models\Inscripcion;
use Packages\Bittacora\FormularioInscripcion\Requests\EnviarFormulario;
use Packages\Bittacora\FormularioInscripcion\src\Inscripciones\InscripcionesRepository;
use Packages\Bittacora\FormularioInscripcion\src\Thumbs;
use RuntimeException;

/**
 * Fachada para el módulo de inscripciones.
 * @package Packages\Bittacora\FormularioInscripcion
 */
class InscripcionesFacade extends Facade
{
    /** @var InscripcionesRepository */
    private $inscripcionesRepository;
    /**
     * @var Thumbs
     */
    private $thumbs;

    public function __construct(Thumbs $thumbs, InscripcionesRepository $inscripcionesRepository)
    {
        $this->thumbs = $thumbs;
        $this->thumbs->setPath(storage_path('app'));
        $this->inscripcionesRepository = $inscripcionesRepository;
    }
    

    public function obtenerInscripciones(bool $incluirInactivas = false): Collection
    {
        return $this->inscripcionesRepository->obtenerInscripciones($incluirInactivas);
    }

    /**
     * @param array<string,string> $formData
     * @param FormRequest $request
     * @psalm-suppress PossiblyInvalidMethodCall
     * @psalm-suppress PossiblyNullReference
     */
    public function guardarInscripcion(array $formData, FormRequest $request): bool
    {
        return $this->inscripcionesRepository->guardarInscripcion($formData, $request);
    }
}
