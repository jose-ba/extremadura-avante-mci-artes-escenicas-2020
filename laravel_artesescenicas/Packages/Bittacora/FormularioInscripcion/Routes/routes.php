<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use Packages\Bittacora\FormularioInscripcion\Controllers\BpanelInscripcionesController;
use Packages\Bittacora\FormularioInscripcion\Controllers\FormularioInscripcionController;
use Packages\Bittacora\FormularioInscripcion\Controllers\InscripcionesController;

Route::prefix('formulario-inscripcion')->group(function () {
    Route::get('/formulario', [FormularioInscripcionController::class, 'show'])->name('formulario.mostrar');
    Route::post('/enviar', [FormularioInscripcionController::class, 'store'])->name('formulario.enviar');
});

Route::prefix('inscripciones')->group(function () {
    Route::get('/{inscripcion}/mostrar', [InscripcionesController::class, 'show'])->name('inscripciones.mostrar');
});

// Rutas solo para usuarios logueados
Route::group(['middleware' => ['web', 'auth']], function () {
    Route::prefix('bpanel/inscripciones')->group(function () {
        Route::get('/listar', [BpanelInscripcionesController::class, 'listar'])->name('inscripciones.listar');
        Route::get('{inscripcion}/editar', [BpanelInscripcionesController::class, 'editar'])
            ->name('inscripciones.editar');
        Route::get('{inscripcion}/cambiarActivo', [BpanelInscripcionesController::class, 'cambiarActivo'])
            ->name('inscripciones.cambiarActivo');
        Route::get('/generarPdf', [BpanelInscripcionesController::class, 'generarPdf'])
            ->name('inscripciones.generarPdf');
        Route::put('/actualizar', [BpanelInscripcionesController::class, 'actualizar'])
            ->name('inscripciones.actualizar');
    });
});
