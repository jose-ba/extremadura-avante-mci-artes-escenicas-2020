@extends('layout')

@section('content')
    <h1>Formulario de inscripción</h1>
    @if(!empty($errors->all()))
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="post" action="{{ route('formulario.enviar') }}" enctype="multipart/form-data">
        @csrf
        <p>Rellenar todos los campos posibles. Esta información será utilizada para elaborar el dossier que se envíe a los importadores, para su selección.</p>
        <p>Una vez iniciado este formulario deberá realizarse completo en una vez, dado que los datos no quedarán guardados. Gracias.</p>
        <div class="card mb-5">
            <div class="card-body">
                <h2 class="card-title">Información de contacto <span class="traduccion">Contact details</span></h2>
                <div class="card-text">
                    <div class="form-group">
                        <label for="empresa" class="required">Empresa <span class="traduccion">Company name</span></label>
                        <input type="text" maxlength="100" name="empresa" id="empresa" class="form-control" required value="{{ old('empresa') }}">
                    </div>
                    <div class="form-group">
                        <label for="persona-contacto" class="required">Persona de contacto <span class="traduccion">Contact person</span></label>
                        <input type="text" maxlength="100" name="persona_contacto" id="persona-contacto" class="form-control" required value="{{ old('persona_contacto') }}">
                    </div>
                    <div class="form-group">
                        <label for="cargo" class="required">Cargo <span class="small">Position</span></label>
                        <input type="text" maxlength="100" name="cargo" id="cargo" class="form-control" required value="{{ old('cargo') }}">
                    </div>
                    <div class="form-group">
                        <label for="web" class="required">Web</label>
                        <input type="url" name="web" id="web" class="form-control" required value="{{ old('web') }}">
                    </div>
                    <hr>
                    <h3>Redes sociales <span class="traduccion">Social networks</span></h3>

                    <div class="form-group row">
                        <div class="col-2">
                            <label for="facebook"><strong>Facebook</strong></label>
                        </div>
                        <div class="col-10">
                            <input type="url" name="facebook" id="facebook" class="form-control" value="{{ old('facebook') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <label for="twitter"><strong>Twitter</strong></label>
                        </div>
                        <div class="col-10">
                            <input type="url" name="twitter" id="twitter" class="form-control" value="{{ old('twitter') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <label for="youtube"><strong>YouTube</strong></label>
                        </div>
                        <div class="col-10">
                            <input type="url" name="youtube" id="youtube" class="form-control" value="{{ old('youtube') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <label for="instagram"><strong>Instagram</strong></label>
                        </div>
                        <div class="col-10">
                            <input type="url" name="instagram" id="instagram" class="form-control" value="{{ old('instagram') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <label for="pinterest"><strong>Pinterest</strong></label>
                        </div>
                        <div class="col-10">
                            <input type="url" name="pinterest" id="pinterest" class="form-control" value="{{ old('pinterest') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-5">
            <div class="card-body">
                <h2 class="card-title">Información sobre su empresa <span class="traduccion">Company details</span></h2>
                <div class="card-text">
                    <div class="form-group">
                        <label for="sector-actividad" class="required">Sector principal de su actividad <span class="traduccion">(Main sector)</span></label>
                        <input type="text" maxlength="100" name="sector_actividad" id="sector-actividad" class="form-control" required value="{{ old('sector_actividad') }}">
                    </div>
                    <div class="form-group">
                        <label for="principales-mercados">Principales Mercados a los que exporta <span class="traduccion">(Main Export International Markets)</span></label>
                        <input type="text" maxlength="100" name="principales_mercados" id="principales-mercados" class="form-control" required value="{{ old('principales_mercados') }}">
                    </div>
                    <div class="form-group">
                        <label for="mercados-exclusividad" class="required">Mercados con los que tiene exclusividad <span class="traduccion">(Exclusive Contract Markets )</span></label>
                        <input type="text" maxlength="100" name="mercados_exclusividad" id="mercados-exclusividad" class="form-control" required value="{{ old('mercados_exclusividad') }}">
                    </div>
                    <div class="form-group">
                        <label for="marcas" class="required">Marcas/producciones teatrales<span class="traduccion">(Brands/Productions)</span></label>
                        <input type="text" maxlength="100" name="marcas" id="marcas" class="form-control" required value="{{ old('marcas') }}">
                    </div>
                    <div class="form-group">
                        <label>¿Porque elegir a mi empresa? <span class="traduccion">(Why choosing my company?)</span></label>
                        <p>Redactar este texto en español e inglés (máx 80 palabras por idioma).</p>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="por-que-elegir-mi-empresa-es" class="required">Español:</label>
                                <textarea class="form-control" name="por_que_elegir_mi_empresa_es" id="por-que-elegir-mi-empresa-es" rows="8" required>{{ old('por_que_elegir_mi_empresa_es') }}</textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="por-que-elegir-mi-empresa-en" class="required">English:</label>
                                <textarea class="form-control" name="por_que_elegir_mi_empresa_en" id="por-que-elegir-mi-empresa-en" rows="8" required>{{ old('por_que_elegir_mi_empresa_es') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-5">
            <div class="card-body">
                <h2 class="card-title">Actividad principal <span class="traduccion">(Main activity)</span></h2>
                <p><span class="traduccion">(por favor, marque la(s) casilla(s) con una "x") / (please mark the box (es) with an "x")</span></p>
                <div class="card-text">
                    <div class="form-check">
                        <label class="form-check-label" for="companias_teatro">
                            <strong>Compañías de teatro <span class="traduccion">(Theater company)</span></strong>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="companias_teatro_danza" name="companias_teatro_danza" @if(old('companias_teatro_danza')) checked @endif>
                        <label class="form-check-label" for="companias_teatro_danza">
                            Danza <span class="traduccion">(Dance)</span>

                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="companias_teatro_musical" name="companias_teatro_musical" @if(old('companias_teatro_musical')) checked @endif>
                        <label class="form-check-label" for="companias_teatro_musical">
                            Musical <span class="traduccion">(Musical)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="companias_teatro_drama" name="companias_teatro_drama" @if(old('companias_teatro_drama')) checked @endif>
                        <label class="form-check-label" for="companias_teatro_drama">
                            Drama <span class="traduccion">(Drama)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="companias_teatro_intriga" name="companias_teatro_intriga" @if(old('companias_teatro_intriga')) checked @endif>
                        <label class="form-check-label" for="companias_teatro_intriga">
                            Intriga <span class="traduccion">(Thriller)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="companias_teatro_comedia" name="companias_teatro_comedia" @if(old('companias_teatro_comedia')) checked @endif>
                        <label class="form-check-label" for="companias_teatro_comedia">
                            Comedia <span class="traduccion">(Comedy)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="companias_teatro_literario" name="companias_teatro_literario" @if(old('companias_teatro_literario')) checked @endif>
                        <label class="form-check-label" for="companias_teatro_literario">
                            Literario <span class="traduccion">(Literary)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="companias_teatro_historico" name="companias_teatro_historico" @if(old('companias_teatro_historico')) checked @endif>
                        <label class="form-check-label" for="companias_teatro_historico">
                            Historico <span class="traduccion">(Historic)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="nombre_campo" name="companias_teatro_infantil" @if(old('companias_teatro_infantil')) checked @endif>
                        <label class="form-check-label" for="companias_teatro_infantil">
                            Infantil <span class="traduccion">(Children's)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="companias_teatro_acrobatico" name="companias_teatro_acrobatico" @if(old('companias_teatro_acrobatico')) checked @endif>
                        <label class="form-check-label" for="companias_teatro_acrobatico">
                            Acrobático <span class="traduccion">(Acrobatic)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="companias_teatro_otros" name="companias_teatro_otros" @if(old('companias_teatro_otros')) checked @endif>
                        <label class="form-check-label" for="companias_teatro_otros">
                            Otros (por favor, indíquelo) <span class="traduccion">(Others, please, specify)</span>
                            <input type="text" maxlength="100" class="form-control" name="companias_teatro_otros_texto" value="{{ old('companias_teatro_otros_texto') }}">
                        </label>
                    </div>

                    <!-- --------------------------------------------------------------------------------------------------------------------------------------------- -->
                    <hr>
                    <!-- --------------------------------------------------------------------------------------------------------------------------------------------- -->

                    <div class="form-check">
                        <label class="form-check-label" for="distribuidores_teatro">
                            <strong>Distribuidores de teatro <span class="traduccion">(Theater distributors)</span></strong>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="distribuidores_teatro_danza" name="distribuidores_teatro_danza" @if(old('distribuidores_teatro_danza')) checked @endif>
                        <label class="form-check-label" for="distribuidores_teatro_danza">
                            Danza <span class="traduccion">(Dance)</span>

                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="distribuidores_teatro_musical" name="distribuidores_teatro_musical" @if(old('distribuidores_teatro_musical')) checked @endif>
                        <label class="form-check-label" for="distribuidores_teatro_musical">
                            Musical <span class="traduccion">(Musical)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="distribuidores_teatro_drama" name="distribuidores_teatro_drama" @if(old('distribuidores_teatro_drama')) checked @endif>
                        <label class="form-check-label" for="distribuidores_teatro_drama">
                            Drama <span class="traduccion">(Drama)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="distribuidores_teatro_intriga" name="distribuidores_teatro_intriga" @if(old('distribuidores_teatro_intriga')) checked @endif>
                        <label class="form-check-label" for="distribuidores_teatro_intriga">
                            Intriga <span class="traduccion">(Thriller)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="distribuidores_teatro_comedia" name="distribuidores_teatro_comedia" @if(old('distribuidores_teatro_comedia')) checked @endif>
                        <label class="form-check-label" for="distribuidores_teatro_comedia">
                            Comedia <span class="traduccion">(Comedy)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="distribuidores_teatro_literario" name="distribuidores_teatro_literario" @if(old('distribuidores_teatro_literario')) checked @endif>
                        <label class="form-check-label" for="distribuidores_teatro_literario">
                            Literario <span class="traduccion">(Literary)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="distribuidores_teatro_historico" name="distribuidores_teatro_historico" @if(old('distribuidores_teatro_historico')) checked @endif>
                        <label class="form-check-label" for="distribuidores_teatro_historico">
                            Historico <span class="traduccion">(Historic)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="nombre_campo" name="distribuidores_teatro_infantil" @if(old('distribuidores_teatro_infantil')) checked @endif>
                        <label class="form-check-label" for="distribuidores_teatro_infantil">
                            Infantil <span class="traduccion">(Children's)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="distribuidores_teatro_acrobatico" name="distribuidores_teatro_acrobatico" @if(old('distribuidores_teatro_acrobatico')) checked @endif>
                        <label class="form-check-label" for="distribuidores_teatro_acrobatico">
                            Acrobático <span class="traduccion">(Acrobatic)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="distribuidores_teatro_otros" name="distribuidores_teatro_otros" @if(old('distribuidores_teatro_otros')) checked @endif>
                        <label class="form-check-label" for="distribuidores_teatro_otros">
                            Otros (por favor, indíquelo) <span class="traduccion">(Others, please, specify)</span>
                            <input type="text" maxlength="100" class="form-control" name="distribuidores_teatro_otros_texto" value="{{ old('distribuidores_teatro_otros_texto') }}">
                        </label>
                    </div>


                    <!-- --------------------------------------------------------------------------------------------------------------------------------------------- -->
                    <hr>
                    <!-- --------------------------------------------------------------------------------------------------------------------------------------------- -->


                    <div class="form-check">
                        <label class="form-check-label" for="programadores_festivales">
                            <strong>Programadores de festivales <span class="traduccion">(Festival programmers)</span></strong>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="programadores_festivales_danza" name="programadores_festivales_danza" @if(old('programadores_festivales_danza')) checked @endif>
                        <label class="form-check-label" for="programadores_festivales_danza">
                            Danza <span class="traduccion">(Dance)</span>

                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="programadores_festivales_musical" name="programadores_festivales_musical" @if(old('programadores_festivales_musical')) checked @endif>
                        <label class="form-check-label" for="programadores_festivales_musical">
                            Musical <span class="traduccion">(Musical)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="programadores_festivales_drama" name="programadores_festivales_drama" @if(old('programadores_festivales_drama')) checked @endif>
                        <label class="form-check-label" for="programadores_festivales_drama">
                            Drama <span class="traduccion">(Drama)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="programadores_festivales_intriga" name="programadores_festivales_intriga" @if(old('programadores_festivales_intriga')) checked @endif>
                        <label class="form-check-label" for="programadores_festivales_intriga">
                            Intriga <span class="traduccion">(Thriller)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="programadores_festivales_comedia" name="programadores_festivales_comedia" @if(old('programadores_festivales_comedia')) checked @endif>
                        <label class="form-check-label" for="programadores_festivales_comedia">
                            Comedia <span class="traduccion">(Comedy)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="programadores_festivales_literario" name="programadores_festivales_literario" @if(old('programadores_festivales_literario')) checked @endif>
                        <label class="form-check-label" for="programadores_festivales_literario">
                            Literario <span class="traduccion">(Literary)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="programadores_festivales_historico" name="programadores_festivales_historico" @if(old('programadores_festivales_historico')) checked @endif>
                        <label class="form-check-label" for="programadores_festivales_historico">
                            Historico <span class="traduccion">(Historic)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="nombre_campo" name="programadores_festivales_infantil" @if(old('programadores_festivales_infantil')) checked @endif>
                        <label class="form-check-label" for="programadores_festivales_infantil">
                            Infantil <span class="traduccion">(Children's)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="programadores_festivales_acrobatico" name="programadores_festivales_acrobatico" @if(old('programadores_festivales_acrobatico')) checked @endif>
                        <label class="form-check-label" for="programadores_festivales_acrobatico">
                            Acrobático <span class="traduccion">(Acrobatic)</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="programadores_festivales_otros" name="programadores_festivales_otros" @if(old('programadores_festivales_otros')) checked @endif>
                        <label class="form-check-label" for="programadores_festivales_otros">
                            Otros (por favor, indíquelo) <span class="traduccion">(Others, please, specify)</span>
                            <input type="text" maxlength="100" class="form-control" name="programadores_festivales_otros_texto" value="{{ old('programadores_festivales_otros_texto') }}">
                        </label>
                    </div>
                    <hr>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="otros" name="otros" @if(old('otros')) checked @endif>
                        <label class="form-check-label" for="otros">
                            Otros (por favor, indíquelo) <span class="traduccion">(Others, please, specify)</span>
                            <input type="text" maxlength="100" class="form-control" name="otros_texto" value="{{ old('otros_texto') }}">
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-5">
            <div class="card-body">
                <h2 class="card-title">Imágenes <span class="traduccion">(Images)</span></h2>
                <div class="card-text">
                    <div class="form-group">
                        <label for="logo" class="required">Logo o imagen del grupo (logo or band image)</label>
                        <input type="file" class="form-control-file" id="logo" name="logo" accept=".png,.jpg,.jpeg">
                        <span class="traduccion"><strong>Tamaño mínimo (minimum size): 800x800px.<br>Se permiten imagenes de hasta 5MB (Images of up to 5MB are allowed).</strong></span>
                    </div>
                    <div class="form-group">
                        <label for="imagen">Puede añadir otra imagen relacionada con la empresa <span class="traduccion">(You can add another related image)</span></label>
                        <input type="file" class="form-control-file" id="imagen" name="imagen" accept=".png,.jpg,.jpeg">
                        <span class="traduccion"><strong>Tamaño mínimo (minimum size): 1000x1000px<br>Se permiten imagenes de hasta 5MB (Images of up to 5MB are allowed).</strong></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label  class="required"><input type="checkbox" name="privacidad" id="privacidad" value="1" required> He leido y acepto la <a href="#popup-privacidad" class="js-popup-open">política de privacidad</a></label>
        </div>

        <button type="submit" class="btn btn-primary">Enviar</button>

    </form>
@endsection
