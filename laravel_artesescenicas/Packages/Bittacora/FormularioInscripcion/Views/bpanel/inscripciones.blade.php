@extends('bpanel.layout-bpanel')

@section('content')

    <div class="ibox-content table-responsive">
        <table class="table table-hover no-margins">
            <thead>
            <tr>
                <th></th>
                <th>Empresa</th>
                <th>Persona de contacto</th>
                <th>Cargo</th>
                <th class="text-center">Web</th>
                <th>Sector actividad</th>
                <th>Principales mercados</th>
                <th class="text-center">Editar</th>
                <th class="text-center"></th>
            </tr>
            </thead>
            <tbody>
            @forelse($inscripciones as $inscripcion)
                <tr>
                    <td class="align-middle text-center"><img class="miniatura-listado" src="{{ asset('storage/' . $inscripcion->thumbs['logo'][1]) }}"></td>
                    <td class="align-middle">{{ $inscripcion->empresa }}</td>
                    <td class="align-middle">{{ $inscripcion->persona_contacto }}</td>
                    <td class="align-middle">{{ $inscripcion->cargo }}</td>
                    <td class="align-middle text-center"><a href="{{ $inscripcion->web }}" target="_blank"><i class="fa fa-globe"></i></a></td>
                    <td class="align-middle">{{ $inscripcion->sector_actividad }}</td>
                    <td class="align-middle">{{ $inscripcion->principales_mercados }}</td>
                    <td class="align-middle text-center"><a href="{{ route('inscripciones.editar', ['inscripcion' => $inscripcion]) }}">Editar</a></td>
                    <td class="align-middle text-center">
                        @if($inscripcion->activo == 1)
                            <a href="{{ route('inscripciones.cambiarActivo', ['inscripcion' => $inscripcion]) }}" title="Desactivar inscripción">
                                <button type="button" class="btn btn-success"><i class="fa fa-power-off"></i></button>
                            </a>
                        @else
                            <a href="{{ route('inscripciones.cambiarActivo', ['inscripcion' => $inscripcion]) }}" title="Activar inscripción">
                                <button type="button" class="btn btn-danger"><i class="fa fa-power-off"></i></button>
                            </a>
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="9" class="text-center">Todavía no se ha recibido ninguna inscripción.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>

@endsection    
