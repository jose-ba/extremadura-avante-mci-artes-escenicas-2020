@extends('layout')


@section('content')
    <div class="ficha-individual">
        <div>
            <h1 class=" animate__animated animate__paused animate__fadeIn animate__delay-1s">{{ $inscripcion->empresa }}</h1>
        </div>

        <img class="logo animate__animated animate__paused animate__fadeIn mb-3" src="{{ asset('storage/' . $inscripcion->thumbs['logo'][2]) }}" alt="Logo de {{ $inscripcion->empresa }}">

        <h2 class="animate__animated animate__paused animate__fadeIn">Información de contacto</h2>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>Persona de contacto:</strong> {{  $inscripcion->persona_contacto }}</p>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>Cargo:</strong> {{  $inscripcion->cargo }}</p>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>Web:</strong> {{  $inscripcion->web }}</p>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>Facebook:</strong> <a href="{{ $inscripcion->facebook }}" target="_blank">{{  $inscripcion->facebook }}</a></p>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>Twitter:</strong> <a href="{{  $inscripcion->twitter }}" target="_blank">{{  $inscripcion->twitter }}</a></p>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>YouTube:</strong> <a href="{{  $inscripcion->youtube }}" target="_blank">{{  $inscripcion->youtube }}</a></p>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>Instagram:</strong> <a href="{{  $inscripcion->instagram }}" target="_blank">{{  $inscripcion->instagram }}</a></p>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>Pinterest:</strong> <a href="{{  $inscripcion->pinterest }}" target="_blank">{{  $inscripcion->pinterest }}</a></p>
        <h2 class="animate__animated animate__paused animate__fadeIn">Información sobre la empresa</h2>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>Sector principal de su actividad:</strong> {{  $inscripcion->sector_actividad }}</p>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>Principales mercados a los que exporta:</strong> {{  $inscripcion->principales_mercados }}</p>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>Mercados con los que tiene exclusividad:</strong> {{  $inscripcion->mercados_exclusividad }}</p>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>Marcas/producciones teatrales: </strong>{{  $inscripcion->marcas}}</p>
        <p class="animate__animated animate__paused animate__fadeIn"><strong>¿Por qué elegir a {{ $inscripcion->empresa }}?</strong>
        <div class="row">
            <div class="col-md-6">{{  $inscripcion->por_que_elegir_mi_empresa_es }}</div>
            <div class="col-md-6">{{  $inscripcion->por_que_elegir_mi_empresa_en }}</div>
        </div>
        @if($inscripcion->companias_teatro_danza or
    $inscripcion->companias_teatro_musical or
    $inscripcion->companias_teatro_drama or
    $inscripcion->companias_teatro_intriga or
    $inscripcion->companias_teatro_comedia or
    $inscripcion->companias_teatro_literario or
    $inscripcion->companias_teatro_historico or
    $inscripcion->companias_teatro_infantil or
    $inscripcion->companias_teatro_acrobatico or
    $inscripcion->companias_teatro_otros
)
            <h2 class="animate__animated animate__paused animate__fadeIn mt-3">Compañías de teatro:</h2>
            <ul class="animate__animated animate__paused animate__fadeIn">
                @if($inscripcion->companias_teatro_danza)
                    <li>Danza <span class="traduccion">(Dance)</span></li>
                @endif
                @if($inscripcion->companias_teatro_musical)
                    <li>Musical <span class="traduccion">(Musical)</span></li>
                @endif
                @if($inscripcion->companias_teatro_drama)
                    <li>Drama <span class="traduccion">(Drama)</span></li>
                @endif
                @if($inscripcion->companias_teatro_intriga)
                    <li>Intriga <span class="traduccion">(Thriller)</span></li>
                @endif
                @if($inscripcion->companias_teatro_comedia)
                    <li>Comedia <span class="traduccion">(Comedy)</span></li>
                @endif
                @if($inscripcion->companias_teatro_literario)
                    <li>Literario <span class="traduccion">(Literary)</span></li>
                @endif
                @if($inscripcion->companias_teatro_historico)
                    <li>Historico <span class="traduccion">(Historic)</span></li>
                @endif
                @if($inscripcion->companias_teatro_infantil)
                    <li>Infantil <span class="traduccion">(Children's)</span></li>
                @endif
                @if($inscripcion->companias_teatro_acrobatico)
                    <li>Acrobático <span class="traduccion">(Acrobatic)</span></li>
                @endif
                @if($inscripcion->companias_teatro_otros)
                    <li>{{ $inscripcion->companias_teatro_otros_texto }}</li>
                @endif
            </ul>
        @endif
        @if($inscripcion->distribuidores_teatro_danza or
    $inscripcion->distribuidores_teatro_musical or
    $inscripcion->distribuidores_teatro_drama or
    $inscripcion->distribuidores_teatro_intriga or
    $inscripcion->distribuidores_teatro_comedia or
    $inscripcion->distribuidores_teatro_literario or
    $inscripcion->distribuidores_teatro_historico or
    $inscripcion->distribuidores_teatro_infantil or
    $inscripcion->distribuidores_teatro_acrobatico or
    $inscripcion->distribuidores_teatro_otros
)
            <h2 class="animate__animated animate__paused animate__fadeIn mt-3">Distribuidores de teatro:</h2>
            <ul class="animate__animated animate__paused animate__fadeIn">
                @if($inscripcion->distribuidores_teatro_danza)
                    <li>Danza <span class="traduccion">(Dance)</span></li>
                @endif
                @if($inscripcion->distribuidores_teatro_musical)
                    <li>Musical <span class="traduccion">(Musical)</span></li>
                @endif
                @if($inscripcion->distribuidores_teatro_drama)
                    <li>Drama <span class="traduccion">(Drama)</span></li>
                @endif
                @if($inscripcion->distribuidores_teatro_intriga)
                    <li>Intriga <span class="traduccion">(Thriller)</span></li>
                @endif
                @if($inscripcion->distribuidores_teatro_comedia)
                    <li>Comedia <span class="traduccion">(Comedy)</span></li>
                @endif
                @if($inscripcion->distribuidores_teatro_literario)
                    <li>Literario <span class="traduccion">(Literary)</span></li>
                @endif
                @if($inscripcion->distribuidores_teatro_historico)
                    <li>Historico <span class="traduccion">(Historic)</span></li>
                @endif
                @if($inscripcion->distribuidores_teatro_infantil)
                    <li>Infantil <span class="traduccion">(Children's)</span></li>
                @endif
                @if($inscripcion->distribuidores_teatro_acrobatico)
                    <li>Acrobático <span class="traduccion">(Acrobatic)</span></li>
                @endif
                @if($inscripcion->distribuidores_teatro_otros)
                    <li>{{ $inscripcion->distribuidores_teatro_otros_texto }}</li>
                @endif
            </ul>
        @endif
        @if($inscripcion->programadores_festivales_danza or
    $inscripcion->programadores_festivales_musical or
    $inscripcion->programadores_festivales_drama or
    $inscripcion->programadores_festivales_intriga or
    $inscripcion->programadores_festivales_comedia or
    $inscripcion->programadores_festivales_literario or
    $inscripcion->programadores_festivales_historico or
    $inscripcion->programadores_festivales_infantil or
    $inscripcion->programadores_festivales_acrobatico or
    $inscripcion->programadores_festivales_otros
)
            <h2 class="animate__animated animate__paused animate__fadeIn mt-3">Programadores de festivales:</h2>
            <ul class="animate__animated animate__paused animate__fadeIn">
                @if($inscripcion->programadores_festivales_danza)
                    <li>Danza <span class="traduccion">(Dance)</span></li>
                @endif
                @if($inscripcion->programadores_festivales_musical)
                    <li>Musical <span class="traduccion">(Musical)</span></li>
                @endif
                @if($inscripcion->programadores_festivales_drama)
                    <li>Drama <span class="traduccion">(Drama)</span></li>
                @endif
                @if($inscripcion->programadores_festivales_intriga)
                    <li>Intriga <span class="traduccion">(Thriller)</span></li>
                @endif
                @if($inscripcion->programadores_festivales_comedia)
                    <li>Comedia <span class="traduccion">(Comedy)</span></li>
                @endif
                @if($inscripcion->programadores_festivales_literario)
                    <li>Literario <span class="traduccion">(Literary)</span></li>
                @endif
                @if($inscripcion->programadores_festivales_historico)
                    <li>Historico <span class="traduccion">(Historic)</span></li>
                @endif
                @if($inscripcion->programadores_festivales_infantil)
                    <li>Infantil <span class="traduccion">(Children's)</span></li>
                @endif
                @if($inscripcion->programadores_festivales_acrobatico)
                    <li>Acrobático <span class="traduccion">(Acrobatic)</span></li>
                @endif
                @if($inscripcion->programadores_festivales_otros)
                    <li>{{ $inscripcion->programadores_festivales_otros_texto }}</li>
                @endif
            </ul>
        @endif
        @if($inscripcion->otros)
            <h2 class="animate__animated animate__paused animate__fadeIn mt-3">Otros:</h2>
            {{ $inscripcion->otros_texto }}
        @endif
        @if(isset($inscripcion->thumbs['imagen']))
            <img class="imagen animate__animated animate__paused animate__fadeIn" src="{{ asset('storage/' . $inscripcion->thumbs['imagen'][2]) }}" alt="Imagen de {{ $inscripcion->empresa }}">
        @endif
    </div>
@endsection


