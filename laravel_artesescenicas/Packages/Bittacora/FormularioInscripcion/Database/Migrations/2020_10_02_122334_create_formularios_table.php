<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormulariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('inscripciones', function (Blueprint $table) {
            $table->id();
            $table->string('empresa');
            $table->string('persona_contacto');
            $table->string('cargo');
            $table->string('web');
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('youtube')->nullable();
            $table->string('instagram')->nullable();
            $table->string('pinterest')->nullable();
            $table->string('sector_actividad');
            $table->string('principales_mercados');
            $table->string('mercados_exclusividad');
            $table->string('marcas');
            $table->text('por_que_elegir_mi_empresa_es');
            $table->text('por_que_elegir_mi_empresa_en');
            $table->tinyInteger('companias_teatro_danza')->default(0);
            $table->tinyInteger('companias_teatro_musical')->default(0);
            $table->tinyInteger('companias_teatro_drama')->default(0);
            $table->tinyInteger('companias_teatro_intriga')->default(0);
            $table->tinyInteger('companias_teatro_comedia')->default(0);
            $table->tinyInteger('companias_teatro_literario')->default(0);
            $table->tinyInteger('companias_teatro_historico')->default(0);
            $table->tinyInteger('companias_teatro_infantil')->default(0);
            $table->tinyInteger('companias_teatro_acrobatico')->default(0);
            $table->tinyInteger('companias_teatro_otros')->default(0);
            $table->string('companias_teatro_otros_texto')->nullable();
            $table->tinyInteger('distribuidores_teatro_danza')->default(0);
            $table->tinyInteger('distribuidores_teatro_musical')->default(0);
            $table->tinyInteger('distribuidores_teatro_drama')->default(0);
            $table->tinyInteger('distribuidores_teatro_intriga')->default(0);
            $table->tinyInteger('distribuidores_teatro_comedia')->default(0);
            $table->tinyInteger('distribuidores_teatro_literario')->default(0);
            $table->tinyInteger('distribuidores_teatro_historico')->default(0);
            $table->tinyInteger('distribuidores_teatro_infantil')->default(0);
            $table->tinyInteger('distribuidores_teatro_acrobatico')->default(0);
            $table->tinyInteger('distribuidores_teatro_otros')->default(0);
            $table->string('distribuidores_teatro_otros_texto')->nullable();
            $table->tinyInteger('programadores_festivales_danza')->default(0);
            $table->tinyInteger('programadores_festivales_musical')->default(0);
            $table->tinyInteger('programadores_festivales_drama')->default(0);
            $table->tinyInteger('programadores_festivales_intriga')->default(0);
            $table->tinyInteger('programadores_festivales_comedia')->default(0);
            $table->tinyInteger('programadores_festivales_literario')->default(0);
            $table->tinyInteger('programadores_festivales_historico')->default(0);
            $table->tinyInteger('programadores_festivales_infantil')->default(0);
            $table->tinyInteger('programadores_festivales_acrobatico')->default(0);
            $table->tinyInteger('programadores_festivales_otros')->default(0);
            $table->string('programadores_festivales_otros_texto')->nullable();
            $table->tinyInteger('otros')->default(0);
            $table->string('otros_texto')->nullable();
            $table->string('logo')->nullable();
            $table->string('imagen')->nullable();
            $table->tinyInteger('privacidad')->default(0);
            $table->tinyInteger('activo')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('inscripciones');
    }
}
