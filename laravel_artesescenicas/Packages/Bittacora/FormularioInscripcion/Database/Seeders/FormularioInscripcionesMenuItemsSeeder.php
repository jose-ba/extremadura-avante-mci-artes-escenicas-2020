<?php

declare(strict_types=1);

namespace Packages\Bittacora\FormularioInscripcion\Database\Seeders;

use Illuminate\Database\Seeder;
use Packages\Bittacora\AdminMenu\Models\AdminMenu;
use Packages\Bittacora\AdminMenu\Models\AdminMenuItem;

/**
 * Class FormularioInscripcionesMenuItemsSeeder
 * @package Packages\Bittacora\FormularioInscripcion\Database\Seeders
 */
class FormularioInscripcionesMenuItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sidebarAdminMenu = AdminMenu::where('slug', 'sidebar')->firstOrFail();
        $parent = AdminMenuItem::where('title', 'Configuración bPanel')->firstOrFail();

        $parentIdAdminMenuItem = AdminMenuItem::create([
            'menu_id' => (int)$sidebarAdminMenu->id,
            'parent_id' => (int)$parent->id,
            'active' => true,
            'title' => 'Inscripciones',
            'route_name' => 'inscripciones.listar',
            'permission' => '',
            'show_in_menu' => true,
            'show_in_dashboard' => true,
            'icon' => 'fa fa-list',
            'css_class' => ''
        ]);
    }
}
