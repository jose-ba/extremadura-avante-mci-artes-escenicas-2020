<?php
declare(strict_types=1);


namespace Packages\Bittacora\FormularioInscripcion\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Packages\Bittacora\FormularioInscripcion\InscripcionesFacade;
use Packages\Bittacora\FormularioInscripcion\Models\Inscripcion;
use Packages\Bittacora\FormularioInscripcion\src\Thumbs;

class InscripcionesController
{
    /**
     * @param Inscripcion $inscripcion
     * @param InscripcionesFacade $inscripcionesFacade
     * @return View
     */
    public function show(Inscripcion $inscripcion, Thumbs $thumbs): View
    {
        $thumbs->addThumbsToModel($inscripcion);
        return view('formulario-inscripcion::fichaInscripcion', ['inscripcion' => $inscripcion]);
    }
}
