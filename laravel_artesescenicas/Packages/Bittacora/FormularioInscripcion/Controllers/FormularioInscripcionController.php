<?php

declare(strict_types=1);

namespace Packages\Bittacora\FormularioInscripcion\Controllers;

use App\Http\Controllers\Controller;
use Packages\Bittacora\FormularioInscripcion\InscripcionesFacade;
use Packages\Bittacora\FormularioInscripcion\Requests\EnviarFormulario;

/**
 * Controlador para el formulario de inscripción.
 * @package Packages\Bittacora\FormularioInscripcion\Controllers
 */
class FormularioInscripcionController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view('formulario-inscripcion::formulario');
    }

    /**
     * @param EnviarFormulario $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(EnviarFormulario $request, InscripcionesFacade $inscripcionesFacade)
    {
        /** @var array<string,string> $formData */
        $formData = $request->except(['_token']);
        $inscripcionesFacade->guardarInscripcion($formData, $request);
        return view('formulario-inscripcion::formularioOk');
    }
}
