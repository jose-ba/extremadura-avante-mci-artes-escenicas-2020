<?php

declare(strict_types=1);

namespace Packages\Bittacora\FormularioInscripcion\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Packages\Bittacora\FormularioInscripcion\InscripcionesFacade;
use Packages\Bittacora\FormularioInscripcion\Models\Inscripcion;
use Packages\Bittacora\FormularioInscripcion\Requests\ActualizarInscripcion;
use Packages\Bittacora\FormularioInscripcion\src\PdfGenerator\PdfGeneratorModule;
use Packages\Bittacora\FormularioInscripcion\src\Thumbs;

class BpanelInscripcionesController
{

    /**
     * @var InscripcionesFacade
     */
    private $inscripcionesFacade;
    /**
     * @var Thumbs
     */
    private $thumbs;

    public function __construct(
        InscripcionesFacade $inscripcionesFacade,
        Thumbs $thumbs
    ) {
        $this->inscripcionesFacade = $inscripcionesFacade;
        $this->thumbs = $thumbs;
    }

    /**
     * @return View
     */
    public function listar(): View
    {
        $inscripciones = $this->inscripcionesFacade->obtenerInscripciones(true);

        return view('formulario-inscripcion::bpanel.inscripciones', [
            'inscripciones' => $inscripciones,
            'pageTitle' => 'Listado de inscripciones'
        ]);
    }

    /**
     * @param Inscripcion $inscripcion
     * @return View
     */
    public function editar(Inscripcion $inscripcion): View
    {
        $this->thumbs->addThumbsToModel($inscripcion);

        return view('formulario-inscripcion::bpanel.editar', [
            'inscripcion' => $inscripcion,
            'pageTitle' => 'Editando inscripción "' . (string)$inscripcion->empresa . '"'
        ]);
    }

    public function actualizar(
        ActualizarInscripcion $request
    ): RedirectResponse {
        /** @var  array<string, string> $input */
        $input = $request->input();
        if ($this->inscripcionesFacade->guardarInscripcion($input, $request)) {
            return back()->with('info', 'Inscripción actualizada');
        }
        return back()->withErrors('Ocurrió un error al actualizar la inscripción');
    }

    /**
     * @param Inscripcion $inscripcion
     * @return RedirectResponse
     */
    public function cambiarActivo(Inscripcion $inscripcion): RedirectResponse
    {
        $inscripcion->activo = 1 - $inscripcion->activo;
        $inscripcion->save();
        return back()->with('info', 'Inscripción actualizada');
    }

    public function generarPdf(PdfGeneratorModule $pdfGeneratorModule): void
    {
        $inscriptions = [];
        $inscriptionsCollection = $this->inscripcionesFacade->obtenerInscripciones();
        /** @var Inscripcion $item */
        foreach ($inscriptionsCollection as $item) {
            $inscription = $item->toArray();
            $inscription['thumbs'] = $item->thumbs;
            $inscriptions[] = $inscription;
        }

        /** @var Inscripcion[] $inscriptions */
        $pdfGeneratorModule->generatePdf($inscriptions);
    }
}
