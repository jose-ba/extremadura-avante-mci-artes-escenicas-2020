<?php

declare(strict_types=1);

namespace Packages\Bittacora\FormularioInscripcion\src;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Constraint;
use Intervention\Image\ImageManager;

/**
 * Clase para generar las miniaturas de las inscripciones
 * @package Packages\Bittacora\FormularioInscripcion\src
 */
class Thumbs
{
    /**
     * @var ImageManager
     */
    private $imageManager;

    /** @var string * */
    protected $path;

    public function __construct(ImageManager $imageManager)
    {
        $this->imageManager = $imageManager;
        $this->path = storage_path() . '/app/public/';
    }

    /**
     * Añade las miniaturas al modelo.
     * @param Model $model
     * @param int $width
     * @param int $height
     * @psalm-suppress MixedArrayAssignment
     * @psalm-suppress MixedArrayAccess
     */
    public function addThumb(Model $model, int $width = 200, int $height = 200): void
    {
        if (!isset($model->thumbs) or !is_array($model->thumbs)) {
            $model->thumbs = []; /* @phpstan-ignore-line */
        }
        $this->generateLogoThumb($model, $width, $height);
        $this->generateImageThumb($model, $width, $height);
    }

    public function thumbFromPath(string $originalImagePath, int $width = 200, int $height = 200): string
    {
        $originalImageRelativePath = str_replace(public_path() . '/storage', '', $originalImagePath);
        $thumbFileName = $this->generateThumbFileName($originalImageRelativePath, $width, $height);
        $this->imageManager->make($originalImagePath)->fit($width, $height)->save($thumbFileName);
        /** @var string $url */
        $url = url('').'/storage';
        $thumbFileName = str_replace($this->path, $url, $thumbFileName);
        return $thumbFileName;
    }

    /**
     * Establece la carpeta base para los archivos generados.
     * @param string $filesPath
     */
    public function setPath(string $filesPath): void
    {
        if (substr($filesPath, -1) !== '/') {
            $filesPath .= '/';
        }
        $this->path = $filesPath;
    }

    /**
     * Añade la propiedad $thumbs al modelo con las miniaturas disponibles.
     * @param Model $model
     * @return void
     */
    public function addThumbsToModel(Model $model): void
    {
        $this->addThumb($model);
        $this->addThumb($model, 30, 30);
        $this->addThumb($model, 1000, 1000);
    }

    /**
     * @param string $thumbFileName
     * @param string $imagePath
     * @param int $width
     * @param int $height
     * @return void
     */
    private function generateThumb(string $thumbFileName, string $imagePath, int $width, int $height): void
    {
        if (!file_exists($thumbFileName)) {
            $this->imageManager->make($this->path . $imagePath)
                ->resize($width, $height, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                })->save($thumbFileName);
        }
    }

    /**
     * @param string $filename
     * @param int $width
     * @param int $height
     * @return string
     */
    private function generateThumbFileName(string $filename, int $width, int $height): string
    {
        $fileParts = explode('/', $filename);
        $originalFileName = (string)last($fileParts);
        return $this->path . str_replace(
            $originalFileName,
            'thumb_' . $width . '_' . $height . '_' . $originalFileName,
            $filename
        );
    }

    private function removeBasePath(string $originalPath): string
    {
        return str_replace(storage_path() . '/app/public/', '', $originalPath);
    }

    /**
     * @param Model $model
     * @param int $width
     * @param int $height
     * @return string
     * @psalm-suppress MixedArrayAccess
     * @psalm-suppress MixedArrayAssignment
     */
    private function generateLogoThumb(Model $model, int $width, int $height): string
    {
        $thumbFileName = '';
        if (!empty($model->logo) and is_string($model->logo)) {
            $thumbFileName = $this->generateThumbFileName($model->logo, $width, $height);
            $this->generateThumb($thumbFileName, $model->logo, $width, $height);
            $model->thumbs['logo'][] = $this->removeBasePath($thumbFileName); /* @phpstan-ignore-line */
        }
        return $thumbFileName;
    }

    /**
     * @param Model $model
     * @param int $width
     * @param int $height
     * @psalm-suppress MixedArrayAccess
     * @psalm-suppress MixedArrayAssignment
     */
    private function generateImageThumb(Model $model, int $width, int $height): void
    {
        if (!empty($model->imagen) and is_string($model->imagen)) {
            $thumbFileName = $this->generateThumbFileName($model->imagen, $width, $height);
            $this->generateThumb($thumbFileName, $model->imagen, $width, $height);
            $model->thumbs['imagen'][] = $this->removeBasePath($thumbFileName); /* @phpstan-ignore-line */
        }
    }
}
