<?php

declare(strict_types=1);

namespace Packages\Bittacora\FormularioInscripcion\src\Inscripciones;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Packages\Bittacora\FormularioInscripcion\Models\Inscripcion;
use Packages\Bittacora\FormularioInscripcion\src\Thumbs;
use Packages\Bittacora\Shared\FormulariosHelper;
use RuntimeException;

/**
 * Class InscripcionesRepository
 * @package Packages\Bittacora\FormularioInscripcion\src\Inscripciones
 */
class InscripcionesRepository
{
    /**
     * @var FormulariosHelper
     */
    private $formulariosHelper;
    /**
     * @var Thumbs
     */
    private $thumbs;

    public function __construct(FormulariosHelper $formulariosHelper, Thumbs $thumbs)
    {
        $this->formulariosHelper = $formulariosHelper;
        $this->thumbs = $thumbs;
    }

    public function obtenerInscripciones(bool $incluirInactivas = false): Collection
    {
        $inscripciones = $incluirInactivas === true ? $inscripciones = Inscripcion::all() :
            Inscripcion::where('activo', 1)->get();

        /** @var Model $inscripcion */
        foreach ($inscripciones as $inscripcion) {
            $this->thumbs->addThumbsToModel($inscripcion);
        }
        /** @psalm-suppress InvalidArgument */
        return new Collection($inscripciones);
    }

    /**
     * @param array $formData
     * @param FormRequest $request
     * @return bool
     * @psalm-suppress PossiblyInvalidArgument
     */
    public function guardarInscripcion(array $formData, FormRequest $request): bool
    {
        if (!empty($formData['id'])) {
            $inscripcion = Inscripcion::where('id', $formData['id'])->firstOrFail();
        } else {
            $inscripcion = new Inscripcion();
        }

        $formData = $this->formulariosHelper->corregirClavesArrayFormulario($formData);

        $inscripcion->fill($formData)->save();
        $requestLogo = $request->file('logo');
        $requestImage = $request->file('imagen');

        if (!empty($requestLogo)) {
            $inscripcion->logo = str_replace('public/', '', $this->guardarFichero($requestLogo));
        }

        if(!empty($requestImage)) {
            $inscripcion->imagen = str_replace('public/','',(string)$this->guardarFichero($requestImage));
        }

        return $inscripcion->save();
    }

    /**
     * @param UploadedFile|null $fichero
     * @return string|null
     */
    private function guardarFichero(?UploadedFile $fichero): ?string
    {
        $path = null;
        if (\is_object($fichero) and is_a($fichero, UploadedFile::class)) {
            $path = $fichero->store('public');
            if (!is_string($path)) {
                throw new RuntimeException('Error al guardar el fichero.');
            }
        }
        return $path;
    }
}
