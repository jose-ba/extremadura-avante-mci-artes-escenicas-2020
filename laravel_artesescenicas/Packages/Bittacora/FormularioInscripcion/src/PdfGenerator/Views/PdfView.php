<?php
/** @var array $inscriptions */

if (!function_exists('corregirLineHeight')) {
    function corregirLineHeight(string $texto): void
    {
        if (strlen($texto) >= 60) {
            echo 'corregir-line-height';
        }
    }
}

if (!function_exists('disminuirFuente')) {
    function disminuirFuente(string $texto, int $limite = 100): void
    {
        if (strlen($texto) > $limite) {
            echo 'disminuir-fuente';
        }
    }
}

?>
<html>
<head>
    <style>

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        .page {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            position: relative;
            background-repeat: no-repeat;
            background-position: center;
            background-size: 100%;
            font-family: sans-serif;
            font-weight: normal;
            font-style: normal;
            font-variant: normal;
            font-size: 2.7mm;
            page-break-inside:avoid;
        }

        .inscripcion {
            position: relative;
        }

        .campo {
            position: absolute;
            width: 79.8mm;
            padding: 0.45mm 2.5mm;
            height: 6.5mm;
            line-height: 7.5mm;
            vertical-align: middle;

        }

        .redSocial {
            color: white;
            position: absolute;
            width: 7.55cm;
            height: .75cm;
            line-height: .75cm;
            padding: 0 0 0 1.15cm;
            vertical-align: middle;
        }

        .redSocial span {
            height: .75cm;
            line-height: .75cm;
            vertical-align: middle;
            top: 0;
            bottom: 0;
            display: block;
            position: absolute;
            padding-right: .2cm;
            width: 100%;
        }

        .columna-izquierda {
            left: 10.8mm;
        }

        .columna-derecha {
            left: 111mm;
        }

        .columna-derecha.campo {
            width: 8.6cm;
            background: rgba(255, 255, 255, 1);

        }

        .columna-derecha.campo.campo-precio {
            width: 32mm;
            height: auto;
            padding: 0;
            line-height: 1;
            background: white;
            font-size: 4mm;
        }

        .texto-largo {
            color: #000;
            background: #f0f0f0;
            position: absolute;
            padding: 1mm;
            top: 51.3mm;
            line-height: 1;
            width: 42.0mm;
            height: 52.4mm;
            text-align: justify;
        }

        .icono-rrss {
            position: absolute;
            top: 171.5mm;
        }

        .icono-rrss img {
            width: 6.9mm;
            height: 6.9mm;
        }

        .logo {
            position: absolute;
            top: 18.9mm;
            left: 27.5mm;
            height: 51mm;
            width: 51mm;
            background-size: contain;
            background-position: center center;
            background-repeat: no-repeat;
            background-color: black;
        }

        .imagen {
            position: absolute;
            top: 227mm;
            left: 115.6mm;
            height: 65mm;
            width: 85.5mm;
            background-size: contain;
            background-position: bottom center;
            background-repeat: no-repeat;
            background-color: white;
        }

        .corregir-line-height {
            line-height: 1;
            padding-top: 1mm;
        }

        .disminuir-fuente {
            font-size: 2.2mm;
            padding-top: 1mm;
        }

        .checkbox {
            width: 2.5mm;
            height: 2.4mm;
        }

    </style>
</head>
<body>
<div id="portada" class="page" style="background-image: url('data:image/jpeg;base64,<?php echo base64_encode(file_get_contents(__DIR__ . '/../../../assets/img/dossier-1.jpg')); ?>');"></div>


<?php
/**
 * @var array{
 *      thumbs: array{logo: array, imagen: array|null},
 *      empresa: string,
 *      cargo: string,
 *      web: string,
 *      persona_contacto: string,
 *      facebook:string,
 *      twitter:string,
 *      youtube:string,
 *      instagram:string,
 *      pinterest:string,
 *      sector_actividad:string,
 *      principales_mercados:string,
 *      mercados_exclusividad:string,
 *      marcas_producciones:string,
 *      por_que_elegir_mi_empresa_es:string,
 *      por_que_elegir_mi_empresa_en:string,
 *      banda_musical:int,
 *      solista:int,
 *      grupos_musicales:int,
 *      organizador_eventos:int,
 *      programador_musical:int,
 *      agro_otros:int,
 *      agro_otros_texto:string,
 * } $inscription
 */
foreach ($inscriptions as $inscription) {
    $logoPath = storage_path('app/public/' . (string)$inscription["thumbs"]["logo"][2]);

    $imagePath = null;
    if (isset($inscription["thumbs"]["imagen"])) {
        $imagePath = storage_path('app/public/' . (string)$inscription["thumbs"]["imagen"][2]);
    } ?>

    <div class="inscripcion page" style="background-image: url('data:image/jpeg;base64,<?php echo
    base64_encode(file_get_contents(__DIR__ . '/../../../assets/img/dossier-2.jpg')); ?>');">
        <div class="logo" style="background-image: url('data:image/png;base64,<?php echo
        base64_encode(file_get_contents($logoPath)); ?>');"></div>
        <div class="campo columna-izquierda" style="top: 96mm;"><?php echo (string)$inscription['empresa']; ?></div>
        <div class="campo columna-izquierda" style="top: 113mm;"><?php echo (string)$inscription['persona_contacto']; ?></div>
        <div class="campo columna-izquierda" style="top: 129.5mm;"><?php echo (string)$inscription['cargo']; ?></div>
        <div class="campo columna-izquierda" style="top: 147.7mm;"><?php echo (string)$inscription['web']; ?></div>
        <?php
        $left = 1.15; ?>
        <?php if ($inscription['facebook'] != '') { ?>
            <div class="icono-rrss" style="top: 17cm; left: <?php echo $left; ?>cm;">
                <a href="<?php echo $inscription['facebook']; ?>"><img src="data:image/png;base64,<?php echo
                    base64_encode(file_get_contents(__DIR__ . '/../../../assets/img/facebook.png')); ?>"></a>
            </div>
            <?php $left += 1;
        } ?>
        <?php if ($inscription['twitter'] != '') { ?>
            <div class="icono-rrss" style="top: 17cm; left: <?php echo $left; ?>cm;">
                <a href="<?php echo $inscription['twitter']; ?>"><img src="data:image/png;base64,<?php echo base64_encode(file_get_contents(__DIR__ . '/../../../assets/img/twitter.png')); ?>"></a></div>
            <?php $left += 1;
        } ?>
        <?php if ($inscription['youtube'] != '') { ?>
            <div class="icono-rrss" style="top: 17cm; left: <?php echo $left; ?>cm;">
                <a href="<?php echo $inscription['youtube']; ?>"><img src="data:image/png;base64,<?php echo base64_encode(file_get_contents(__DIR__ . '/../../../assets/img/youtube.png')); ?>"></a></div>
            <?php $left += 1;
        } ?>
        <?php if ($inscription['instagram'] != '') { ?>
            <div class="icono-rrss" style="top: 17cm; left: <?php echo $left; ?>cm;">
                <a href="<?php echo $inscription['instagram']; ?>"><img src="data:image/png;base64,<?php echo base64_encode(file_get_contents(__DIR__ . '/../../../assets/img/instagram.png')); ?>"></a></div>
            <?php $left += 1;
        } ?>
        <?php if ($inscription['pinterest'] != '') { ?>
            <div class="icono-rrss" style="top: 17cm; left: <?php echo $left; ?>cm;">
                <a href="<?php echo $inscription['pinterest']; ?>"><img src="data:image/png;base64,<?php echo base64_encode(file_get_contents(__DIR__ . '/../../../assets/img/pinterest.png')); ?>"></a></div>
            <?php $left += 1;
        } ?>
        <div class="campo columna-izquierda <?php corregirLineHeight($inscription['sector_actividad']); ?>" style="top: 201.9mm;"><?php echo $inscription['sector_actividad']; ?></div>
        <div class="campo columna-izquierda <?php corregirLineHeight($inscription['principales_mercados']); ?>" style="top: 221.3mm;"><?php echo $inscription['principales_mercados']; ?></div>
        <div class="campo columna-izquierda <?php corregirLineHeight($inscription['mercados_exclusividad']); ?>" style="top: 242.7mm;"><?php echo $inscription['mercados_exclusividad']; ?></div>
        <div class="campo columna-derecha <?php corregirLineHeight($inscription['marcas']); ?>" style="top: 22.5mm;"><?php echo $inscription['marcas']; ?></div>

        <div class="columna-derecha texto-largo <?php disminuirFuente($inscription['por_que_elegir_mi_empresa_es'], 700); ?>" style="left:113.4mm"><?php echo $inscription['por_que_elegir_mi_empresa_es']; ?></div>
        <div class="columna-derecha texto-largo  <?php disminuirFuente($inscription['por_que_elegir_mi_empresa_en'], 700); ?>" style="left: 159.9mm;"><?php echo $inscription['por_que_elegir_mi_empresa_en']; ?></div>
        <?php if ($inscription['companias_teatro_danza']) { ?>
            <div class="checkbox" style="position: absolute;background: #000; top:130.1mm;left:113.5mm;width: .2cm;height:.2cm"></div>
        <?php } ?>
        <?php if ($inscription['companias_teatro_musical']) { ?>
            <div class="checkbox" style="position: absolute;background: #000; top:133.7mm;left:113.5mm;width: .2cm;height:.2cm"></div>
        <?php } ?>
        <?php if ($inscription['companias_teatro_drama']) { ?>
            <div class="checkbox" style="position: absolute;background: #000; top:137.3mm;left:113.5mm;width: .2cm;height:.2cm"></div>
        <?php } ?>
        <?php if ($inscription['companias_teatro_intriga']) { ?>
            <div class="checkbox" style="position: absolute;background: #000; top:140.6mm;left:113.5mm;width: .2cm;height:.2cm"></div>
        <?php } ?>
        <?php if ($inscription['companias_teatro_comedia']) { ?>
            <div class="checkbox" style="position: absolute;background: #000; top:144.1mm;left:113.5mm;width: .2cm;height:.2cm"></div>
        <?php } ?>
        <?php if ($inscription['companias_teatro_literario']) { ?>
            <div class="checkbox" style="position: absolute;background: #000; top:147.7mm;left:113.5mm;width: .2cm;height:.2cm"></div>
        <?php } ?>
        <?php if ($inscription['companias_teatro_historico']) { ?>
            <div class="checkbox" style="position: absolute;background: #000; top:130.1mm;left:156.2mm;width: .2cm;height:.2cm"></div>
        <?php } ?>
        <?php if ($inscription['companias_teatro_infantil']) { ?>
            <div class="checkbox" style="position: absolute;background: #000; top:133.4mm;left:156.2mm;width: .2cm;height:.2cm"></div>
        <?php } ?>
        <?php if ($inscription['companias_teatro_acrobatico']) { ?>
            <div class="checkbox" style="position: absolute;background: #000; top:137mm;left:156.2mm;width: .2cm;height:.2cm"></div>
        <?php } ?>
        <?php if ($inscription['companias_teatro_otros']) { ?>
            <div class="checkbox" style="position: absolute;background: #000; top:140.6mm;left:156.2mm;width: .2cm;height:.2cm"></div>
            <div style="position: absolute; top: 147.1mm; left: 160mm;width:45mm;"><?php echo $inscription['companias_teatro_otros_texto']; ?></div>
        <?php } ?>
        <div style="position: fixed; top: 36.35mm; left: 0; width: 210mm;"> <!-- Uso este div para añadir un "offset" a los checkbox que contiene -->

            <?php if ($inscription['distribuidores_teatro_danza']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:130.1mm;left:113.5mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['distribuidores_teatro_musical']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:133.7mm;left:113.5mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['distribuidores_teatro_drama']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:137.3mm;left:113.5mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['distribuidores_teatro_intriga']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:140.6mm;left:113.5mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['distribuidores_teatro_comedia']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:144.1mm;left:113.5mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['distribuidores_teatro_literario']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:147.7mm;left:113.5mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['distribuidores_teatro_historico']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:130.1mm;left:156.2mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['distribuidores_teatro_infantil']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:133.4mm;left:156.2mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['distribuidores_teatro_acrobatico']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:137mm;left:156.2mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['distribuidores_teatro_otros']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:140.6mm;left:156.2mm;width: .2cm;height:.2cm"></div>
                <div style="position: absolute; top: 147.1mm; left: 160mm;width:45mm; "><?php echo $inscription['distribuidores_teatro_otros_texto']; ?></div>
            <?php } ?>
        </div>
        <div style="position: fixed; top: 70.8mm; left: 0; width: 210mm;"> <!-- Uso este div para añadir un "offset" a los checkbox que contiene -->

            <?php if ($inscription['programadores_festivales_danza']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:130.1mm;left:113.5mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['programadores_festivales_musical']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:133.7mm;left:113.5mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['programadores_festivales_drama']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:137.3mm;left:113.5mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['programadores_festivales_intriga']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:140.6mm;left:113.5mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['programadores_festivales_comedia']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:144.1mm;left:113.5mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['programadores_festivales_literario']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:147.7mm;left:113.5mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['programadores_festivales_historico']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:130.1mm;left:156.2mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['programadores_festivales_infantil']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:133.4mm;left:156.2mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['programadores_festivales_acrobatico']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:137mm;left:156.2mm;width: .2cm;height:.2cm"></div>
            <?php } ?>
            <?php if ($inscription['programadores_festivales_otros']) { ?>
                <div class="checkbox" style="position: absolute;background: #000; top:140.6mm;left:156.2mm;width: .2cm;height:.2cm"></div>
                <div style="position: absolute; top: 147.1mm; left: 160mm;width:45mm; "><?php echo $inscription['programadores_festivales_otros_texto']; ?></div>
            <?php } ?>
        </div>
        <?php if ($inscription['otros']) { ?>
<!--            <div class="checkbox" style="position: absolute;background: #000; top:189mm;left:114.1mm;width: .2cm;height:.2cm"></div>-->
<!--            <div style="position: absolute; top: 191.3mm; left: 117.9mm;">--><?php //echo $inscription['otros_texto']; ?><!--</div>-->
        <?php } ?>
        <?php if (isset($imagePath)) { ?>
            <div class="imagen" style="background-image: url('data:image/png;base64,<?php echo base64_encode(file_get_contents($imagePath)); ?>');"></div>
        <?php } else { ?>
            <div class="imagen"></div>
        <?php } ?>
    </div>
    <?php
} ?>
<div id="trasera" class="page" style="background-image: url('data:image/jpeg;base64,<?php echo base64_encode(file_get_contents(__DIR__ . '/../../../assets/img/dossier-3.jpg')); ?>');"></div></body></html>
