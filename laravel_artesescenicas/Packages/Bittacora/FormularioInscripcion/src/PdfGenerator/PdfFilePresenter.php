<?php

declare(strict_types=1);

namespace Packages\Bittacora\FormularioInscripcion\src\PdfGenerator;

use Dompdf\Dompdf;
use Dompdf\Options;

class PdfFilePresenter
{
    public function generatePdf(string $html = ''): void
    {
        $options = new Options();
        $options->setDpi(300);
        $dompdf = new Dompdf($options);
        $dompdf->setPaper('A4', 'portrait');

        // Quito cualquier espacio en blanco entre etiquetas para evitar páginas en blanco que aparecen si no hago esto

        $dompdf->loadHtml(trim($html));
        $dompdf->render();
        $dompdf->stream('dompdf_out.pdf', ['Attachment' => false]);
    }
}

