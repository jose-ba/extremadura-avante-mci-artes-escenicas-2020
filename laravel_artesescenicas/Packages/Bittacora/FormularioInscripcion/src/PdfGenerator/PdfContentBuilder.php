<?php

declare(strict_types=1);

namespace Packages\Bittacora\FormularioInscripcion\src\PdfGenerator;

use Packages\Bittacora\FormularioInscripcion\InscripcionesFacade;
use Packages\Bittacora\FormularioInscripcion\Models\Inscripcion;

/**
 * Clase que se encarga de generar los contenidos que luego se pasarán al PDF.
 * @package Packages\Bittacora\FormularioInscripcion\src\PdfGenerator
 */
class PdfContentBuilder
{

    /**
     * Construye el HTML a partir de un array de inscripciones
     * @param Inscripcion[] $inscriptions
     * @return string
     */
    public function buildHtml(array $inscriptions): string
    {
        ob_start();
        include __DIR__ . '/Views/PdfView.php';
        return (string)ob_get_clean();
    }
}
