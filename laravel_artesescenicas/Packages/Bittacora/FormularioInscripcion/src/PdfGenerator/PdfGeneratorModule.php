<?php

declare(strict_types=1);

namespace Packages\Bittacora\FormularioInscripcion\src\PdfGenerator;

use Packages\Bittacora\FormularioInscripcion\Models\Inscripcion;

/**
 * Fachada para el módulo.
 * @package Packages\Bittacora\FormularioInscripcion\src\PdfGenerator
 */
class PdfGeneratorModule
{
    /**
     * @var PdfContentBuilder
     */
    private $contentBuilder;
    /**
     * @var PdfFilePresenter
     */
    private $filePresenter;

    public function __construct(PdfContentBuilder $contentBuilder, PdfFilePresenter $filePresenter)
    {
        $this->contentBuilder = $contentBuilder;
        $this->filePresenter = $filePresenter;
    }

    /**
     * Genera un pdf a partir de un array de inscripciones.
     * @param Inscripcion[] $inscriptions
     */
    public function generatePdf(array $inscriptions): void
    {
        $html = $this->contentBuilder->buildHtml($inscriptions);
        $this->filePresenter->generatePdf($html);
    }
}
