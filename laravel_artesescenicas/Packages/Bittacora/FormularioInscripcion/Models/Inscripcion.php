<?php

declare(strict_types=1);

namespace Packages\Bittacora\FormularioInscripcion\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Modelo para las inscripciones registradas por el formulario.
 * @package Packages\Bittacora\FormularioInscripcion\Models
 * @psalm-suppress PropertyNotSetInConstructor
 * @method static Model first()
 * @property int $activo
 * @property string $empresa
 * @property string $logo
 * @property string $imagen
*/
class Inscripcion extends Model
{
    use HasFactory;

    public $table = 'inscripciones';

    /** @var array */
    public $thumbs = [];


    public $fillable = [
        'empresa',
        'persona_contacto',
        'cargo',
        'web',
        'facebook',
        'twitter',
        'youtube',
        'instagram',
        'pinterest',
        'sector_actividad',
        'principales_mercados',
        'mercados_exclusividad',
        'marcas',
        'por_que_elegir_mi_empresa_es',
        'por_que_elegir_mi_empresa_en',
        'companias_teatro',
        'companias_teatro_danza',
        'companias_teatro_musical',
        'companias_teatro_drama',
        'companias_teatro_intriga',
        'companias_teatro_comedia',
        'companias_teatro_literario',
        'companias_teatro_historico',
        'companias_teatro_infantil',
        'companias_teatro_acrobatico',
        'companias_teatro_otros',
        'companias_teatro_otros_texto',
        'distribuidores_teatro',
        'distribuidores_teatro_danza',
        'distribuidores_teatro_musical',
        'distribuidores_teatro_drama',
        'distribuidores_teatro_intriga',
        'distribuidores_teatro_comedia',
        'distribuidores_teatro_literario',
        'distribuidores_teatro_historico',
        'distribuidores_teatro_infantil',
        'distribuidores_teatro_acrobatico',
        'distribuidores_teatro_otros',
        'distribuidores_teatro_otros_texto',
        'programadores_festivales',
        'programadores_festivales_danza',
        'programadores_festivales_musical',
        'programadores_festivales_drama',
        'programadores_festivales_intriga',
        'programadores_festivales_comedia',
        'programadores_festivales_literario',
        'programadores_festivales_historico',
        'programadores_festivales_infantil',
        'programadores_festivales_acrobatico',
        'programadores_festivales_otros',
        'programadores_festivales_otros_texto',
        'otros',
        'otros_texto',
        'logo',
        'imagen',
        'privacidad'
    ];
}
