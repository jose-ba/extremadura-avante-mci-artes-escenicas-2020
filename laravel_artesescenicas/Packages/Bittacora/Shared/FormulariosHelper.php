<?php

declare(strict_types=1);

namespace Packages\Bittacora\Shared;

/**
 * Class FormulariosHelper
 * @package Packages\Bittacora\Shared
 */
class FormulariosHelper
{
    /**
     * @param array $formData
     * @return array
     */
    public function corregirClavesArrayFormulario(array $formData): array
    {
        /**
         * @var string $key
         * @var string $value
         */
        foreach ($formData as $key => $value) {
            unset($formData[$key]);
            $newKey = (string)str_replace('-', '_', $key);
            $formData[$newKey] = $value;
        }
        return $formData;
    }
}
