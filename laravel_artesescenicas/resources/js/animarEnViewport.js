"use strict";

/**
 * Script para activar las animaciones de animate.css (https://animate.style/) cuando los elemntos entran en el
 * viewport. Para usarlo, hay que añadir esta clase a los estilos del proyecto:
 *
 * .animate__paused {
 *       animation-play-state: paused;
 * }
 *
 * Esa clase debe añadirse a los elementos que se quieran animar al entrar en el viewport junto con las de animate.style,
 * por ejemplo: animate__animated animate__paused animate__fadeInDown
 *
 * El script quitará esta clase automáticamente cuando el elemento esté en el viewport. Hay que tener en cuenta que para
 * las animaciones que muevan el elemento puede no funcionar bien (habría que cambiar el código que detecta cuándo
 * debería quitarse la clase (isInViewport()).
 *
 */

var isInViewport = function (elem) {
    var bounding = elem.getBoundingClientRect();
    return (
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};

function processElements() {
    var elements = document.querySelectorAll('.animate__paused');
    [].forEach.call(elements, function (element) {
        if (isInViewport(element)) {
            element.classList.remove('animate__paused');
        }
    });
}

window.addEventListener('scroll', function (event) {
    processElements();
}, false);

window.onload = function() {
    processElements();
};
