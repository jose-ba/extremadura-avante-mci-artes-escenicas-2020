@extends('layout')

@section('slider')
    <div class="container-fluid p-0 animate__animated animate__fadeIn animate__delay-1s animate__slow">
        <div id="slick-portada" style="min-height: 30rem; position: relative;">
            @foreach($slides as $slide)
                <img src="{{ $slide  }}" style="min-height: 30rem; object-fit: cover;">
            @endforeach
        </div>
    </div>
@endsection

@section('content')
    <!-- IMPORTADORES -->
    <h1 class="mb-3 text-center">MCI Costa Rica 2020</h1>
    <div class="clearfix">
        <p>Gracias por su interés en Extremadura. Es un placer para nosotros poder presentarle la oferta empresarial de nuestra región durante el evento que se celebrará los días 18 y 20 de noviembre a través del formato virtual.</p>
        <p>Nuestra región tiene una larga tradición mobiliaria, donde empresas del sector aportan un valor y estilo propio, del que nos sentimos muy orgullos en Extremadura.</p>
        <p>Por otro lado, el sector de la cosmética ha crecido en los últimos años con empresas con un gran carácter innovador que han ido incorporando nuevas composiciones, como por ejemplo el aceite de oliva virgen extra, extraído de la aceituna. Este producto es muy característico de nuestra región y tiene una gran diversidad de aplicación.</p>
        <p>A continuación podrá ver información de los participantes en este evento, a partir del cual podrá seleccionar aquellos perfiles que sean de su interés.</p>
    </div>

    <div>
        <h1 class="text-center animate__animated animate__paused animate__fadeIn animate__delay-1s">Empresas - Companies</h1>
    </div>
    <div class="text-center">
        <a href="{{asset('storage/dossier_mci_musica_y_auxiliares.pdf')}}">
            <button class="btn btn-primary">
                Descargar dossier
            </button>
        </a>
    </div>
    <div class="rejilla-empresas">
        @forelse($inscripciones as $inscripcion)
            <div class="empresa animate__animated animate__paused animate__fadeIn">
    <a href="{{ route('inscripciones.mostrar', ['inscripcion' => $inscripcion->id]) }}">
        <img src="{{asset('storage/'.$inscripcion->thumbs['logo'][0])}}" alt="Logo de {{ $inscripcion->empresa }}" class="logo">
        <div class="nombre">{{ $inscripcion->empresa }}</div>
    </a>
    </div>
    @empty
        <p class="text-center w-100">Todavía no se ha realizado ninguna inscripción.</p>
        @endforelse
        </div>
    {{--
    <!-- FORMULARIO-->
    <div class="animate__animated animate__paused animate__fadeInDown">
        <h1 class="text-center">MCI ARTES ESCÉNICAS 2020 - Company Profile</h1>
        <p>Bienvenidos a la página de cumplimentación de datos del perfil de empresa (company profile) para participar en la MCI ARTES ESCÉNICAS 2020, que se celebrará virtualmente del 17 al 20 de noviembre de 2020.</p>
        <p>Extremadura Avante organizará <strong>una agenda de trabajo individualizada con reuniones virtuales</strong>.</p>
        <p>A continuación, encontraréis un formulario a cumplimentar con los datos de vuestros perfiles profesionales, para que la delegación internacional tenga vuestra información.</p>
        <p>Ha de tenerse en cuenta lo siguiente:</p>
        <ol>
            <li>La información recogida en este formulario será enviada a la delegación internacional para que puedan seleccionar las empresas extremeñas con quienes deseen reunirse.</li>
            <li>Tras esta selección, Extremadura Avante notificará a todas las empresas extremeñas inscritas el resultado de dicha valoración:
                <ul>
                    <li>Para las empresas que <strong>NO</strong> hayan sido seleccionadas, se gestionará la devolución de la fianza en un plazo de 60 días naturales.</li>
                    <li>Para las empresas que <strong>SI</strong> hayan sido seleccionadas por uno o más importadores, se continuará informando sobre el correcto desarrollo de la acción.</li>
                </ul>
            </li>
        </ol>
    </div>
    <div class="text-center animate__animated animate__paused animate__fadeIn animate__delay-1s">
        <a href="{{ route('formulario.mostrar') }}">
            <button class="btn btn-primary">
                Ir al formulario
            </button>
        </a>
    </div>
    --}}
@endsection


