<nav class="navbar-default navbar-static-side" role="navigation">

    <div class="sidebar-collapse">
        <div class="contenedor-logo-sidebar">
        <a href="{{ route('dashboard') }}">
            {{--<img src="{{asset('bpanel_assets/assets/img/bpanel_blanco.png')}}" alt="Inicio" title="Inicio" class="logo-sidebar">--}}
            <img src="{{asset('assets/img/LogoAvante-21.png')}}" class="img-fluid w-20 mx-auto">
        </a>
        </div>
        <div class="navegacion-jetstream">
          {{--   @livewire('navigation-dropdown') --}}
        </div>
        
        <ul class="nav metismenu" id="side-menu">
            {{--            <li class="nav-header">--}}
            {{--                <div class="dropdown profile-element">--}}
            {{--                    <img alt="image" class="rounded-circle" src="img/profile_small.jpg"/>--}}
            {{--                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">--}}
            {{--                        <span class="block m-t-xs font-bold">David Williams</span>--}}
            {{--                        <span class="text-muted text-xs block">Art Director <b class="caret"></b></span>--}}
            {{--                    </a>--}}
            {{--                    <ul class="dropdown-menu animated fadeInRight m-t-xs">--}}
            {{--                        <li><a class="dropdown-item" href="profile.html">Profile</a></li>--}}
            {{--                        <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>--}}
            {{--                        <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>--}}
            {{--                        <li class="dropdown-divider"></li>--}}
            {{--                        <li><a class="dropdown-item" href="login.html">Logout</a></li>--}}
            {{--                    </ul>--}}
            {{--                </div>--}}
            {{--                <div class="logo-element">--}}
            {{--                    IN+--}}
            {{--                </div>--}}
            {{--            </li>--}}
            <li><a href="{{ route('bpanel') }}"><i class="fa fa-home"></i><span class="nav-label"> Inicio</span></a></li>
            @forelse($sidebarMenu as $item)
                <li class="{{ $item['css_class'] }}">
                    @if(!empty($item['children']) or !empty($item['route_name']))
                        <a href="#"><i class="{{ $item['icon'] }}">
                            </i> <span class="nav-label">{{ $item['title'] }}</span>
                            @if(!empty($item['children']))<span class="fa arrow"></span>@endif
                        </a>
                        <ul class="nav nav-second-level collapse">
                            @forelse($item['children'] as $child)
                                @if(!empty($child['children']) or !empty($child['route_name']))
                                    <li class="{{ $child['css_class'] }}">
                                        <a href="@if(!empty($child['route_name'])){{ route($child['route_name']) }}@else # @endif">{{$child['title']}}</a>
                                        @if(!empty($child['children']))
                                            <ul class="nav nav-third-level collapse">
                                                @forelse($child['children'] as $subChild)
                                                    <li class="{{ $subChild['css_class'] }}">
                                                        <a href="@if(!empty($subChild['route_name'])){{ route($subChild['route_name']) }}@else # @endif">
                                                            <i class="{{ $subChild['icon'] }}"></i> {{$subChild['title']}}
                                                        </a>
                                                    </li>
                                                @empty

                                                @endforelse
                                            </ul>
                                        @endif
                                    </li>
                                @endif
                            @empty

                            @endforelse
                        </ul>
                    @endif
                </li>
            @empty

            @endforelse
            <li><a href="{{ route('inicio') }}"><i class="fa fa-link"></i><span class="nav-label"> Ir a la web</span></a></li>
        </ul>

    </div>
</nav>
