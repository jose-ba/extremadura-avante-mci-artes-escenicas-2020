<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>@yield('metaTitle', 'bPanel') | {{env('APP_NAME')}}</title>

    {{-- <link href="{{asset('bpanel/assets/css/bootstrap.min.css')}}" rel="stylesheet">--}}
    {{-- <link href="{{asset('bpanel/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet">--}}

    <!-- Toastr style -->
    {{-- <link href="{{asset('bpanel/assets/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">--}}

    <!-- Gritter -->
    {{-- <link href="{{asset('bpanel/assets/js/plugins/gritter/jquery.gritter.css')}}" rel="stylesheet">--}}

    {{-- <link href="{{asset('bpanel/assets/css/animate.css')}}" rel="stylesheet">--}}
    {{-- <link href="{{asset('bpanel/assets/css/style.css')}}" rel="stylesheet"> --}}
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.0/dist/alpine.js"></script>
    <link href="{{asset('bpanel_assets/assets/css/app.css')}}" rel="stylesheet">
    <link href="{{asset('bpanel_assets/assets/css/bpanel.css')}}" rel="stylesheet">
    
    
    @livewireStyles

    @yield('extrasHeader')

</head>


