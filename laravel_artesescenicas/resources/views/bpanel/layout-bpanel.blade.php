{{-- Este template es la base de la que extenderán las vistas del bPanel --}}

@include('bpanel.partials.header')

<body>
<div id="wrapper">

    @include('bpanel.partials.menu')
    <div id="page-wrapper" class="gray-bg dashbard-1">

    @include('bpanel.partials.top-bar')

        @if(!empty($pageTitle))
        
        <div class="row wrapper border-bottom white-bg page-heading mb-3">
            <div class="col-lg-10">
                <h2>{{ $pageTitle }}</h2>
                {{-- <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html">Home</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Layouts</strong>
                    </li>
                </ol>--}}
            </div>
            <div class="col-lg-2">

            </div>
        </div>
        
        @endif

        @if(Session::has('info'))
            <div class="alert alert-primary alert-dismissible">
            {!! \Session::get("info") !!}
            </div>
        @endif


        @yield('content')

        <div class="pt-3 text-right"><small>bPanel 4 &copy; {{date('Y')}} <a href="https://bittacora.com/" target="_blank">bittacora</a></small></div>
    </div>

</div>
@livewireScripts
</body>

@include('bpanel.partials.footer')
