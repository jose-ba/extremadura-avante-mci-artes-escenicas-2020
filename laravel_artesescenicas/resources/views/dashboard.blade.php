@extends('bpanel.layout-bpanel')
@section('content')
    <div class="ibox-content">
        <ul class="iconos-dashboard">
        @forelse($sidebarMenu as $item)
                @if(!empty($item['children']) or !empty($item['route_name']))
            <li>
                    <a href="#"><i class="{{ $item['icon'] }}">
                        </i>{{ $item['title'] }}</span>
                        @if(!empty($item['children']))<span class="fa arrow"></span>@endif
                    </a>
                    <ul class="">
                        @forelse($item['children'] as $child)
                            @if(!empty($child['children']) or !empty($child['route_name']))
                                <li class="{{ $child['css_class'] }}">
                                    <a href="@if(!empty($child['route_name'])){{ route($child['route_name']) }}@else # @endif">
                                    <i class="{{ $child['icon'] }}"></i>
                                    <span>{{$child['title']}}</span>
                                    </a>
                                    @if(!empty($child['children']))
                                        <ul class="">
                                            @forelse($child['children'] as $subChild)
                                                <li class="{{ $subChild['css_class'] }}">
                                                    <a href="@if(!empty($subChild['route_name'])){{ route($subChild['route_name']) }}@else # @endif">
                                                        <i class="{{ $subChild['icon'] }}"></i> {{$subChild['title']}}
                                                    </a>
                                                </li>
                                            @empty

                                            @endforelse
                                        </ul>
                                    @endif
                                </li>
                            @endif
                        @empty

                        @endforelse
                    </ul>
            </li>
                @endif
        @empty
            
        @endforelse
        </ul>
    </div>
@endsection
