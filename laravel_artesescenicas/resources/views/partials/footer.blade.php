<footer class="pie">
    <div class="container">
        <div class="d-flex flex-row flex-wrap">
            <div class="col-md-4 logo-footer">
                <img class="img-fluid "  alt="Fondo Europeo de Desarrollo Regional" src="{{asset('/assets/img/logosUEExtremadura-FEDERnegativo.png')}}">
            </div>
            <div class="col-md-4 logo-footer"> 
                <img class="img-fluid "  alt="Extremadura Avante" src="{{asset('/assets/img/LogoAvante-19.png')}}">
            </div>
            <div class="col-md-4 logo-footer">
                <img class="img-fluid "  alt="Unión Europea" src="{{asset('/assets/img/logosUEExtremadura-negativo.png')}}">
            </div> 
        </div>
        <div class="d-flex flex-row">
            <div class="col-12 text-center">
                <a href="#popup-privacidad" class="btn js-popup-open">política de privacidad</a>
            </div>
        </div>
    </div>
</footer>  
<script src="{{asset('js/publicApp.js')}}"></script>  
<script>
    $(document).ready(function () {  
        $('#slick-portada').slick({ 
            autoplay: true,
            autoplaySpeed: 2000,
        }); 
        $(window).on('load',function () {
            $('.preloader').fadeOut(300);
        });
    });

    window.setTimeout(function(){
        $('.preloader').fadeOut(300);
    }, 3000); 
</script>

@include('partials.politica-privacidad')

</body>
</html>
