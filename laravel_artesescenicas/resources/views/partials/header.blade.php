<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>MCI ARTES ESCÉNICAS 2020</title>
    <link rel="stylesheet" href="{{asset('/assets/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('/bpanel_assets/assets/css/animate.css')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('/assets/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('/assets/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('/assets/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('/assets/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('/assets/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('/assets/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('/assets/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('/assets/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('/assets/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('/assets/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('/assets/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('/assets/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/assets/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('/assets/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('/assets/favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    <!-- /Favicon -->
    <style>
        .preloader {
            background: black;
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: 999;
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            max-width: 100vw;
            height: 100%;
            max-height: 100vh;
        } 
    </style> 
 
</head>
<body>
@if(env('DUSK_RUNNING') != true)  
    <div class="preloader">
        <img src="{{asset('assets/img/LogoAvante-21.png')}}" class="img-fluid animate__animated animate__flash animate__infinite animate__slower">
    </div>
@endif
<header class="cabecera">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-6"><a href="{{ route('inicio') }}"><img class="img-fluid logo" alt="Extremadura Avante" src="{{asset('/assets/img/logo-header.png')}}"></a></div>
            <div class="col-6 justify-content-end align-items-center d-flex flex-row"><a href="{{ route('inicio') }}">Inicio</a></div>
        </div>
    </div>
</header>
