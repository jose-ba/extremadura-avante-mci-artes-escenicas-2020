
    @include('partials/header')

    @yield('slider')
    <div class="container py-5">
        @yield('content')
    </div>
    @include('partials/footer')

