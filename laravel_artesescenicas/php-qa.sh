#!/bin/bash

rutaArchivosConfiguracion=/var/www/html/archivos-de-configuraci-n

# Los analizadores se ejecutan de menos importante a más importante, para poder ver
# rápidamente si hay problemas en Pslm, por ejemplo.

inotifywait -r -m --exclude "(php~|stubphp|\.blade\.php)" --event close_write $(pwd) |
    while read path action file; do
        if [[ "$file" =~ .*php$ ]]; then # Does the file end with .php?
            clear
            echo -e "\n\nEjecutando phpcs\n"
            phpcs -p --colors --extensions=php --standard=PSR2 --ignore=*/tests/*,*vendor/*,*.blade.php,*storage/framework/views*,*/Migrations/*,*/bootstrap/* . 
            echo -e "\n\nEjecutando phpmd\n"
            phpmd $(pwd) ansi --exclude *vendor* $rutaArchivosConfiguracion/phpmdRuleset.xml
            echo -e "Ejecutando psalm\n"
            psalm
        fi
    done
