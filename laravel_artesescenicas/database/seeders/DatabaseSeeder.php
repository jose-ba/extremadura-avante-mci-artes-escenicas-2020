<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Packages\Bittacora\AdminMenu\Database\Seeders\AdminMenuItemSeeder;
use Packages\Bittacora\AdminMenu\Database\Seeders\AdminMenuSeeder;
use Packages\Bittacora\FormularioInscripcion\Database\Seeders\FormularioInscripcionesMenuItemsSeeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'bittacora',
            'email' => 'jose@bittacora.com',
            'password' => Hash::make('mci$eavante2020_'),
        ]);

        $this->call(AdminMenuSeeder::class);
        $this->call(AdminMenuItemSeeder::class);
	$this->call(FormularioInscripcionesMenuItemsSeeder::class);
    }
}
