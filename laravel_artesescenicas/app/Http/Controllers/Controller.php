<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\View\View;
use Packages\Bittacora\FormularioInscripcion\InscripcionesFacade;
use Packages\Bittacora\FormularioInscripcion\src\Thumbs;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(InscripcionesFacade $inscripcionesFacade, Thumbs $thumbs): View
    {
        
        $inscripciones = $inscripcionesFacade->obtenerInscripciones();

        $slides = [
            //$thumbs->thumbFromPath(public_path('storage/placeholder.jpg'), 1910, 490),
            $thumbs->thumbFromPath(public_path('storage/slide-1.jpg'), 1910, 490),
            $thumbs->thumbFromPath(public_path('storage/slide-2.jpg'), 1910, 490),
//            $thumbs->thumbFromPath(public_path('storage/slider-2.jpg'), 1910, 490),
//            $thumbs->thumbFromPath(public_path('storage/slider-3.jpg'), 1910, 490),
        ];


        return view('index', [
            'inscripciones' => $inscripciones,
            'slides' => $slides
        ]);
    }
}
