<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MaxWordsRule implements Rule
{
    /**
     * @var int
     */
    private $maxWords;

    /**
     * Create a new rule instance.
     *
     * @param int $maxWords
     */
    public function __construct(int $maxWords = PHP_INT_MAX)
    {
        $this->maxWords = $maxWords;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return str_word_count((string)$value) <= $this->maxWords;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute no puede superar las ' . $this->maxWords . ' palabras.';
    }
}
