<?php

declare(strict_types=1);

namespace Tests\Feature\Packages\Bittacora\FormularioInscripcion\src\Thumbs;

use Illuminate\Support\Facades\Storage;
use Packages\Bittacora\FormularioInscripcion\Models\Inscripcion;
use Packages\Bittacora\FormularioInscripcion\src\Thumbs;
use Tests\TestCase;

class ThumbsTest extends TestCase
{
    /**
     * @var Thumbs
     */
    public $thumbs;

    public function setUp(): void
    {
        parent::setUp();
        $this->thumbs = $this->app->make(Thumbs::class);

        Inscripcion::where('empresa', '::empresa::')->delete();
        $inscripcion = new Inscripcion();
        $inscripcion->fill([
            'empresa' => '::empresa::',
            'persona_contacto' => '::persona_contacto::',
            'cargo' => '::cargo::',
            'web' => 'http://web.com/',
            'facebook' => 'http://facebook.com/',
            'twitter' => 'http://twitter.com/',
            'youtube' => 'http://youtube.com/',
            'instagram' => 'http://instagram.com/',
            'pinterest' => 'http://pinterest.com/',
            'sector_actividad' => '::sector_actividad::',
            'principales_mercados' => '::principales_mercados::',
            'mercados_exclusividad' => '::mercados_exclusividad::',
            'marcas' => '::marcas::',
            'por_que_elegir_mi_empresa_es' => '::por_que_elegir_mi_empresa_es::',
            'por_que_elegir_mi_empresa_en' => '::por_que_elegir_mi_empresa_en::',
            'otros_texto' => '::otros_texto::',
            'companias_teatro_otros_texto' => '::companias_teatro_otros_texto::',
            'distribuidores_teatro_otros_texto' => '::distribuidores_teatro_otros_texto::',
            'programadores_festivales_otros_texto' => '::programadores_festivales_otros_texto::',
            'companias_teatro_danza' => '1',
            'companias_teatro_musical' => '1',
            'companias_teatro_drama' => '1',
            'companias_teatro_intriga' => '1',
            'companias_teatro_comedia' => '1',
            'companias_teatro_literario' => '1',
            'companias_teatro_historico' => '1',
            'companias_teatro_infantil' => '1',
            'companias_teatro_acrobatico' => '1',
            'companias_teatro_otros' => '1',
            'distribuidores_teatro_danza' => '1',
            'distribuidores_teatro_musical' => '1',
            'distribuidores_teatro_drama' => '1',
            'distribuidores_teatro_intriga' => '1',
            'distribuidores_teatro_comedia' => '1',
            'distribuidores_teatro_literario' => '1',
            'distribuidores_teatro_historico' => '1',
            'distribuidores_teatro_infantil' => '1',
            'distribuidores_teatro_acrobatico' => '1',
            'distribuidores_teatro_otros' => '1',
            'programadores_festivales_danza' => '1',
            'programadores_festivales_musical' => '1',
            'programadores_festivales_drama' => '1',
            'programadores_festivales_intriga' => '1',
            'programadores_festivales_comedia' => '1',
            'programadores_festivales_literario' => '1',
            'programadores_festivales_historico' => '1',
            'programadores_festivales_infantil' => '1',
            'programadores_festivales_acrobatico' => '1',
            'programadores_festivales_otros' => '1',
            'otros' => '1',
            'privacidad' => '1',
            'logo' => 'placeholder.jpg'
        ]);
        $inscripcion->save();
    }

    public function testSeGeneraLaMiniaturaYSeAnadeAlModelo()
    {
        $model = Inscripcion::where('empresa', '::empresa::')->firstOrFail();
        $this->thumbs->addThumb($model);
        $this->assertNotEmpty($model->thumbs);
        $url = asset('storage/'.$model->thumbs['logo'][0]);
        $headers = get_headers($url);
        // Compruebo que la URL existe
        $this->assertTrue(strpos($headers[0], '200') === false ? false : true);
    }

    public function testLaRutaDelArchivoEsRelativaALaCarpetaStorage()
    {
        $model = Inscripcion::first();
        $this->thumbs->addThumb($model);
        $this->assertNotEmpty($model->thumbs);
        $this->assertStringNotContainsString(storage_path(), $model->thumbs['logo'][0]);
    }
}
