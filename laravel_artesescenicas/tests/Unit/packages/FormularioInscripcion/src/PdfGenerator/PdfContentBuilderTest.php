<?php

declare(strict_types=1);

use Packages\Bittacora\FormularioInscripcion\InscripcionesFacade;
use Packages\Bittacora\FormularioInscripcion\Models\Inscripcion;
use Packages\Bittacora\FormularioInscripcion\src\PdfGenerator\PdfContentBuilder;
use Tests\TestCase;

class PdfContentBuilderTest extends TestCase
{
    protected $pdfContentBuilder;

    /**
     * @return Inscripcion
     */
    private function generateInscripcionSample(): Inscripcion
    {
        $inscription = new Inscripcion();

        $inscription->fill([
            'empresa' => '::empresa::',
            'persona_contacto' => '::persona_contacto::',
            'cargo' => '::cargo::',
            'web' => 'http://web.com/',
            'facebook' => 'http://facebook.com/',
            'twitter' => 'http://twitter.com/',
            'youtube' => 'http://youtube.com/',
            'instagram' => 'http://instagram.com/',
            'pinterest' => 'http://pinterest.com/',
            'sector_actividad' => '::sector_actividad::',
            'principales_mercados' => '::principales_mercados::',
            'mercados_exclusividad' => '::mercados_exclusividad::',
            'marcas' => '::marcas::',
            'por_que_elegir_mi_empresa_es' => '::por_que_elegir_mi_empresa_es::',
            'por_que_elegir_mi_empresa_en' => '::por_que_elegir_mi_empresa_en::',
            'otros_texto' => '::otros_texto::',
            'companias_teatro_otros_texto' => '::companias_teatro_otros_texto::',
            'distribuidores_teatro_otros_texto' => '::distribuidores_teatro_otros_texto::',
            'programadores_festivales_otros_texto' => '::programadores_festivales_otros_texto::',
            'companias_teatro_danza' => '1',
            'companias_teatro_musical' => '1',
            'companias_teatro_drama' => '1',
            'companias_teatro_intriga' => '1',
            'companias_teatro_comedia' => '1',
            'companias_teatro_literario' => '1',
            'companias_teatro_historico' => '1',
            'companias_teatro_infantil' => '1',
            'companias_teatro_acrobatico' => '1',
            'companias_teatro_otros' => '1',
            'distribuidores_teatro_danza' => '1',
            'distribuidores_teatro_musical' => '1',
            'distribuidores_teatro_drama' => '1',
            'distribuidores_teatro_intriga' => '1',
            'distribuidores_teatro_comedia' => '1',
            'distribuidores_teatro_literario' => '1',
            'distribuidores_teatro_historico' => '1',
            'distribuidores_teatro_infantil' => '1',
            'distribuidores_teatro_acrobatico' => '1',
            'distribuidores_teatro_otros' => '1',
            'programadores_festivales_danza' => '1',
            'programadores_festivales_musical' => '1',
            'programadores_festivales_drama' => '1',
            'programadores_festivales_intriga' => '1',
            'programadores_festivales_comedia' => '1',
            'programadores_festivales_literario' => '1',
            'programadores_festivales_historico' => '1',
            'programadores_festivales_infantil' => '1',
            'programadores_festivales_acrobatico' => '1',
            'programadores_festivales_otros' => '1',
            'otros' => '1',
            'privacidad' => '1',
            'logo' => 'placeholder.jpg'
        ]);
        return $inscription;
    }


    public function setUp(): void
    {
        $this->pdfContentBuilder = new PdfContentBuilder();
        parent::setUp();
    }

    public function testSeGeneraElHtmlCorrectamenteSiElArrayDeInscripcionesEstaVacio()
    {
        $inscriptions = [];
        $result = $this->pdfContentBuilder->buildHtml($inscriptions);
        $this->assertIsString($result);
        $this->assertStringContainsString('<html>', $result);
        $this->assertStringContainsString('</html>', $result);
        $this->assertStringNotContainsString('class="inscripcion"', $result);
    }

    public function testSeGeneraElNumeroCorrectoDePaginas()
    {
        $inscriptions = [];

        for ($i = 0; $i < 5; $i++) {
            $inscription = $this->generateInscripcionSample();
            $inscriptions[] = $inscription;
        }

        $result = $this->pdfContentBuilder->buildHtml($inscriptions);

        // Compruebo que en el HTML de la página hay 5 inscripciones
        $doc = new DOMDocument();
        $doc->loadHTML($result);
        $selector = new DOMXPath($doc);
        $result = $selector->query('//div[contains(@class,"inscripcion")]');
        $this->assertEquals(5, $result->length);
    }
}
