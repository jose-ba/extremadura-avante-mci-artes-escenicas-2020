<?php

declare(strict_types=1);

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class FormularioTest extends DuskTestCase
{
    protected $formValues = [
        'empresa' => '::empresa::',
        'persona_contacto' => '::persona_contacto::',
        'cargo' => '::cargo::',
        'web' => 'http://web.com/',
        'facebook' => 'http://facebook.com/',
        'twitter' => 'http://twitter.com/',
        'youtube' => 'http://youtube.com/',
        'instagram' => 'http://instagram.com/',
        'pinterest' => 'http://pinterest.com/',
        'sector_actividad' => '::sector_actividad::',
        'principales_mercados' => '::principales_mercados::',
        'mercados_exclusividad' => '::mercados_exclusividad::',
        'marcas' => '::marcas::',
        'por_que_elegir_mi_empresa_es' => '::por_que_elegir_mi_empresa_es::',
        'por_que_elegir_mi_empresa_en' => '::por_que_elegir_mi_empresa_en::',
        'otros_texto' => '::otros_texto::',
        'companias_teatro_otros_texto' => '::companias_teatro_otros_texto::',
        'distribuidores_teatro_otros_texto' => '::distribuidores_teatro_otros_texto::',
        'programadores_festivales_otros_texto' => '::programadores_festivales_otros_texto::',
    ];

    protected $checkBoxes = [
        'companias_teatro_danza' => '1',
        'companias_teatro_musical' => '1',
        'companias_teatro_drama' => '1',
        'companias_teatro_intriga' => '1',
        'companias_teatro_comedia' => '1',
        'companias_teatro_literario' => '1',
        'companias_teatro_historico' => '1',
        'companias_teatro_infantil' => '1',
        'companias_teatro_acrobatico' => '1',
        'companias_teatro_otros' => '1',
        'distribuidores_teatro_danza' => '1',
        'distribuidores_teatro_musical' => '1',
        'distribuidores_teatro_drama' => '1',
        'distribuidores_teatro_intriga' => '1',
        'distribuidores_teatro_comedia' => '1',
        'distribuidores_teatro_literario' => '1',
        'distribuidores_teatro_historico' => '1',
        'distribuidores_teatro_infantil' => '1',
        'distribuidores_teatro_acrobatico' => '1',
        'distribuidores_teatro_otros' => '1',
        'programadores_festivales_danza' => '1',
        'programadores_festivales_musical' => '1',
        'programadores_festivales_drama' => '1',
        'programadores_festivales_intriga' => '1',
        'programadores_festivales_comedia' => '1',
        'programadores_festivales_literario' => '1',
        'programadores_festivales_historico' => '1',
        'programadores_festivales_infantil' => '1',
        'programadores_festivales_acrobatico' => '1',
        'programadores_festivales_otros' => '1',
        'otros' => '1',
        'privacidad' => '1'
    ];

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testNoSePuedeEnviarElFormularioSinAdjuntarLogo()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/formulario-inscripcion/formulario');
            $this->fillForm($browser);
            $browser->press('ENVIAR');
            $browser->assertSee('El campo logo es obligatorio');
        });
    }

    public function testSePuedeEnviarElFormularioYSeGuarda()
    {
        $nombreEmpresa = 'Empresa ' . time();
        $this->browse(function (Browser $browser) use ($nombreEmpresa) {
            $browser->visit('/formulario-inscripcion/formulario');
            $this->formValues['empresa'] = $nombreEmpresa;
            $this->fillForm($browser);
            $browser->attach('logo', base_path('tests/Browser/Formulario/assets/testImage.png'));
            $browser->press('ENVIAR');
            $browser->assertSee('Su inscripción se ha enviado correctamente, muchas gracias.');
        });

        $this->assertDatabaseHas(
            'inscripciones',
            ['empresa' => $nombreEmpresa]
        );
    }

    public function testSeCarganCorrectamenteLasImagenes()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/formulario-inscripcion/formulario');
            $this->fillForm($browser);
            $browser->attach('logo', base_path('tests/Browser/Formulario/assets/testImage.png'));
            $browser->press('ENVIAR');
        });
        $logoPath = DB::table('inscripciones')->latest('logo')->first()->logo;
        $this->assertNotEmpty($logoPath);
        $this->assertFileExists(Storage::path('public/'.$logoPath));
    }

    public function testNoSePuedenRegistrarDosEmpresasConElMismoNombre()
    {
        $this->formValues['empresa'] = 'Empresa ' . time();
        $this->browse(function (Browser $browser) {
            $browser->visit('/formulario-inscripcion/formulario');
            $this->fillForm($browser);
            $browser->attach('logo', base_path('tests/Browser/Formulario/assets/testImage.png'));
            $browser->press('ENVIAR');
            $browser->assertSee('Su inscripción se ha enviado correctamente, muchas gracias.');

            // Se intenta crear otra vez la misma empresa
            $browser->visit('/formulario-inscripcion/formulario');
            $this->fillForm($browser);
            $browser->attach('logo', base_path('tests/Browser/Formulario/assets/testImage.png'));
            $browser->press('ENVIAR');
            $browser->assertSee('El valor del campo empresa ya está en uso.');
            $browser->screenshot('sdfjslj');
        });
    }

    public function testLaPoliticaDePrivacidadEsObligatoria()
    {
        $nombreEmpresa = 'Empresa ' . time();
        $this->browse(function (Browser $browser) use ($nombreEmpresa) {
            $browser->visit('/formulario-inscripcion/formulario');
            $this->formValues['empresa'] = $nombreEmpresa;
            $this->checkBoxes['privacidad'] = 0;
            $this->fillForm($browser);
            $browser->attach('logo', base_path('tests/Browser/Formulario/assets/testImage.png'));
            $browser->press('ENVIAR');
            $browser->assertDontSee('Su inscripción se ha enviado correctamente, muchas gracias.');
        });

        $this->assertDatabaseMissing(
            'inscripciones',
            ['empresa' => $nombreEmpresa]
        );
    }

    protected function fillForm(Browser $browser)
    {
        foreach ($this->formValues as $field => $value) {
            $browser->type($field, $value);
        }
        foreach ($this->checkBoxes as $field => $state) {
            if ($state == 1) {
                $browser->check($field);
            } else {
                $browser->uncheck($field);
            }
        }
    }
}
