<?php

use Laravel\Dusk\Browser;

class BpanelTest extends \Tests\DuskTestCase
{
    /**
     * @var string[] Listado de campos del formulario para poder procesarlos todos en masa.
     */
    public $formFields = [
        'empresa',
        'persona_contacto',
        'cargo',
        'web',
        'facebook',
        'twitter',
        'youtube',
        'instagram',
        'pinterest',
        'sector_actividad',
        'principales_mercados',
        'mercados_exclusividad',
        'marcas',
        'por_que_elegir_mi_empresa_es',
        'por_que_elegir_mi_empresa_en',
        'otros_texto',
        'companias_teatro_otros_texto',
        'distribuidores_teatro_otros_texto',
        'programadores_festivales_otros_texto',
        'companias_teatro_danza',
        'companias_teatro_musical',
        'companias_teatro_drama',
        'companias_teatro_intriga',
        'companias_teatro_comedia',
        'companias_teatro_literario',
        'companias_teatro_historico',
        'companias_teatro_infantil',
        'companias_teatro_acrobatico',
        'companias_teatro_otros',
        'distribuidores_teatro_danza',
        'distribuidores_teatro_musical',
        'distribuidores_teatro_drama',
        'distribuidores_teatro_intriga',
        'distribuidores_teatro_comedia',
        'distribuidores_teatro_literario',
        'distribuidores_teatro_historico',
        'distribuidores_teatro_infantil',
        'distribuidores_teatro_acrobatico',
        'distribuidores_teatro_otros',
        'programadores_festivales_danza',
        'programadores_festivales_musical',
        'programadores_festivales_drama',
        'programadores_festivales_intriga',
        'programadores_festivales_comedia',
        'programadores_festivales_literario',
        'programadores_festivales_historico',
        'programadores_festivales_infantil',
        'programadores_festivales_acrobatico',
        'programadores_festivales_otros',
        'otros'
    ];

    public function tearDown(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->driver->manage()->deleteAllCookies();
        });

        parent::tearDown();
    }

    public function testSePuedenListarLasInscripciones()
    {
        $this->browse(function (\Laravel\Dusk\Browser $browser) {
            $this->hacerLoginComoAdmin($browser);
            $browser->visit('/bpanel/inscripciones/listar');
            $browser->assertSee('Listado de inscripciones');
        });
    }

    public function testNoSePierdeInformacionAlGuardarLaFichaDeUnaInscripcion()
    {
        // NOTA: Esta prueba fallará si no hay una inscripción con ID=1, tengo que hacer que no dependa de eso
        $this->browse(function (\Laravel\Dusk\Browser $browser) {
            $this->hacerLoginComoAdmin($browser);
            $browser->visit('/bpanel/inscripciones/1/editar');
            // Obtengo los valores de todos los campos del formulario, después guardo sin modificar nada y compruebo que
            // no se ha perdido nada (esta prueba la hago porque en algún caso se perdían los checkbox).
            $originalValues = [];
            foreach ($this->formFields as $field) {
                $originalValues[$field] = $browser->value('[name="' . $field.'"]');
            }
            $browser->click('button[type="submit"]');
            // Navego a otra página y vuelvo para que los @old no interfieran
            $browser->visit('/bpanel/');
            $browser->visit('/bpanel/inscripciones/1/editar');
            $newValues = [];
            foreach ($this->formFields as $field) {
                $newValues[$field] = $browser->value('[name="' . $field.'"]');
            }
            $this->assertEquals($originalValues, $newValues);
        });
    }

    /**
     * @param \Laravel\Dusk\Browser $browser
     */
    private function hacerLoginComoAdmin(\Laravel\Dusk\Browser $browser): void
    {
        $browser->visit('/bpanel');
        $browser->type('email', 'jose@bittacora.com');
        $browser->type('password', 'mci$eavante2020_');
        $browser->click('button[type="submit"]');
    }
}
