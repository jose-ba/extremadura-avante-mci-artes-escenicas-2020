#Estructura de directorios
 
 Este proyecto es un Laravel pero en vez de tener la estructura habitual, se ha renombrado /public a /nombreproyecto y el resto se ha movido a laravel_musica. Esto se ha hecho así porque en un mismo subdominio tengo que montar 3 web distintas en subcarpetas de ese subdominio, y así no hay conflictos.
 
 Por lo demás es un Laravel normal.
